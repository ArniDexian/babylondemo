# Babylon code challenge iOS project

## Prerequisites

This code challenge is done for Babylon in 2019, unfortenutaley it's never been checked by them due to closed position.
Here FRP was as an requirement as well as unit tests coverage.

In Jul 2021 resolved the most irritating chunks for me meanwhile putting `NOTE:` comment for every place which I would re-do now,
unit tests haven't been touched/checked

### Technologies
 - CocoaPods dependency tool v1.5.3
 - ReactiveSwift 6.0 / ReactiveCocoa 10 as FRP frameworks
 - Pure UIKit
 
### Environment
 The latest checked iOS version is 12.4, Xcode 12.4
 The iOS version of initial developement was 12.4,  Xcode 10.3
 
### Tests
 There are bunch of Unit/Integrational Tests added to the project
 There are no UI/Snapshots tests due to inital requirements
 
### Improvements
 I placed some comments after  `// NOTE: ` block at places which I would re-do now so they're subject to improve.

### YAGNI principle
I intentionally didn't use constants blindly for every string/number coz I believe they're not needed in such cases.  
 
### Screenshots
 
| Main screen | Main failed   | Details          |
| -------------- | -------------- | -------------- |
|  ![main](Screens/main_success.png) | ![main failed](Screens/main_failed.png) | ![details](Screens/details.png) |
