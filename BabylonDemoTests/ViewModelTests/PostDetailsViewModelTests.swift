//
//  PostDetailsViewModelTests.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 27/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
@testable import BabylonDemo

class PostDetailsViewModelTests: XCTestCase {

    //generating test data
    let post =
            PostTestModel(id: "1", title: "title", body: "post test body description", commentModels: (0..<5).map { j in CommentTestModel(body: "comment \(j)", email: "mail@m.com", id: "\(j)", name: "c name \(j)", postModel: nil)}, userModel: UserTestModel(id: "1", name: "name", email: "mail@m.com", phone: "111-111-111", username: "username", website: nil, addressModel: AddressTestModel(suite: "suite", city: "city", street: "Street 1", zipCode: nil, latitude: nil, longitude: nil, userModel: nil), companyModel: UserCompanyTestModel(bs: nil, catchPhrase: nil, name: "Company", userModel: nil), postModels: nil))

    func testFieldsDisplayDataCorrectly() {
        let viewModel = PostDetailsViewModel(postModel: post)

        XCTAssertEqual(viewModel.title.value, "title")
        XCTAssertEqual(viewModel.body.value, "post test body description")
        XCTAssertEqual(viewModel.commentsText.value, "Comments: 5")
        XCTAssertEqual(viewModel.userName.value, "By name from Company")
    }
}


