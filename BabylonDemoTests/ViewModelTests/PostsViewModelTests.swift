//
//  PostsViewModelTests.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 22/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
import ReactiveSwift
@testable import BabylonDemo

class PostsViewModelTests: XCTestCase {

    //generating test data
    let (users, posts) = { () -> ([UserModel], [PostModel]) in
        let users = (0..<3).map { i in
            UserTestModel(id: "\(i)", name: "name \(i)", email: "mail\(i)@m.com", phone: "111-111-11\(i)", username: "username \(i)", website: nil, addressModel: AddressTestModel(suite: "suite", city: "city \(i)", street: "Street 1", zipCode: nil, latitude: nil, longitude: nil, userModel: nil), companyModel: UserCompanyTestModel(bs: nil, catchPhrase: nil, name: "Company \(i)", userModel: nil), postModels: nil)
        }

        let posts = (0..<10).map { i in
            PostTestModel(id: "\(i)", title: "title \(i)", body: "post test body description", commentModels: (0..<5).map { j in CommentTestModel(body: "comment \(i)_\(j)", email: "mail@m.com", id: "\(j)", name: "c name \(j)", postModel: nil)}, userModel: users[Int.random(in: 0..<users.count)])
        }

        return (users, posts)
    }()

    func testTitleIsSet() {
        let viewModel = postViewModel(fetchedPosts: posts, storedPosts: nil, error: nil)

        XCTAssertTrue(viewModel.title.count > 0)
    }

    func testPostsSynchronisedOnFirstLaunch() {
        //given
        let viewModel = postViewModel(fetchedPosts: posts, storedPosts: nil, error: nil)

        let expectedObservationPostsCount = [0, posts.count]
        let expectedPostTitles = Set(posts.map { $0.title! })

        // when
        let observedExpectation = expectation(description: "posts field is observed")
        observedExpectation.expectedFulfillmentCount = 2

        var observedPostsCount = [Int]()
        viewModel.posts.signal.observeValues { (posts) in
            observedPostsCount.append(posts.count)
            observedExpectation.fulfill()
        }

        viewModel.loadPosts()
        waitForExpectations(timeout: 0.1)

        // then
        XCTAssertNil(viewModel.errorMessage.value)
        XCTAssertEqual(observedPostsCount, expectedObservationPostsCount)
        XCTAssertEqual(Set(viewModel.posts.value.map { $0.title }), expectedPostTitles)
    }

    func testPostsLoadedOfflineAndAfterSynchronised() {
        // given
        let storedPosts = Array(posts[0..<(posts.count/2)])
        let viewModel = postViewModel(fetchedPosts: posts, storedPosts: storedPosts, error: nil)

        let expectedObservationPostsCount = [storedPosts.count, posts.count]
        let expectedPostTitles = Set(posts.map { $0.title! })

        // when
        let observedExpectation = expectation(description: "posts field is observed")
        observedExpectation.expectedFulfillmentCount = 2

        var observedPostsCount = [Int]()
        viewModel.posts.signal.observeValues { (posts) in
            observedPostsCount.append(posts.count)
            observedExpectation.fulfill()
        }

        viewModel.loadPosts()
        waitForExpectations(timeout: 0.1)

        // then
        XCTAssertNil(viewModel.errorMessage.value)
        XCTAssertEqual(observedPostsCount, expectedObservationPostsCount)
        XCTAssertEqual(Set(viewModel.posts.value.map { $0.title }), expectedPostTitles)
    }

    func testPostLoadedOffline() {
        //given
        let viewModel = postViewModel(fetchedPosts: nil, storedPosts: posts, error: NSError.some)

        let expectedPostTitles = Set(posts.map { $0.title! })

        //when
        let observedExpectation = expectation(description: "posts field is observed")
        let notObservedExpectation = expectation(description: "second observation is not occurred")
        notObservedExpectation.isInverted = true

        viewModel.posts.signal.observeValues { (posts) in
            observedExpectation.fulfill()
        }

        viewModel.loadPosts()

        waitForExpectations(timeout: 0.1)

        //then
        XCTAssertNotNil(viewModel.errorMessage.value)
        XCTAssertEqual(Set(viewModel.posts.value.map { $0.title }), expectedPostTitles)
    }

    // correct ordering is descending by post id
    func testReceivedPostsOrderedCorrectly() {
        //given
        let storedPosts = Array(posts[0..<posts.count / 2].shuffled())
        let viewModel = postViewModel(fetchedPosts: posts, storedPosts: storedPosts, error: nil)

        let expectedPostTitles: [[String]] = [
            storedPosts.sorted { $0.id > $1.id }.map { $0.title! },
            posts.sorted { $0.id > $1.id }.map { $0.title! }
        ]

        // when
        let observedExpectation = expectation(description: "expect posts field is observed")
        observedExpectation.expectedFulfillmentCount = 2

        var observedPostTitles = [[String]]()

        viewModel.posts.signal.observeValues { (posts) in
            observedPostTitles.append(posts.map { $0.title })
            observedExpectation.fulfill()
        }

        viewModel.loadPosts()
        waitForExpectations(timeout: 0.1)

        // then
        XCTAssertNil(viewModel.errorMessage.value)
        XCTAssertEqual(observedPostTitles, expectedPostTitles)
    }

    func testIsLoadingOnFirstLaunch() {
        //given
        let viewModel = postViewModel(fetchedPosts: posts, storedPosts: nil, error: nil)

        let expectedIsLoadingObservation = [true, false]

        XCTAssertFalse(viewModel.isLoading.value, "before loading isLoading must be false")

        // when
        let observedExpectation = expectation(description: "expect isLoading field is observed")
        observedExpectation.expectedFulfillmentCount = 2

        var observedValues = [Bool]()
        viewModel.isLoading.signal.observeValues { (value) in
            observedValues.append(value)
            observedExpectation.fulfill()
        }

        viewModel.loadPosts()
        waitForExpectations(timeout: 0.1)

        // then
        XCTAssertEqual(observedValues, expectedIsLoadingObservation)
    }

    func testIsLoadingOnSecondLaunch() {
        //given
        let viewModel = postViewModel(fetchedPosts: posts, storedPosts: posts, error: nil)

        let expectedIsLoadingObservation = [true, false]

        XCTAssertFalse(viewModel.isLoading.value, "before loading isLoading must be false")

        // when
        let observedExpectation = expectation(description: "expect isLoading field is observed")
        observedExpectation.expectedFulfillmentCount = 2

        var observedValues = [Bool]()
        viewModel.isLoading.signal.observeValues { (value) in
            observedValues.append(value)
            observedExpectation.fulfill()
        }

        viewModel.loadPosts()
        waitForExpectations(timeout: 0.1)

        // then
        XCTAssertEqual(observedValues, expectedIsLoadingObservation)
    }

    func testIsLoadingAfterFailure() {
        //given
        let viewModel = postViewModel(fetchedPosts: nil, storedPosts: nil, error: NSError.some)

        // when
        let isLoadingChangedExpectation = expectation(description: "isLoading was set to false")
        let errorObservedExpectation = expectation(description: "some error is observed")

        viewModel.isLoading.signal.observeValues { (value) in
            if !value {
                isLoadingChangedExpectation.fulfill()
            }
        }

        viewModel.errorMessage.signal.observeValues { (value) in
            if value != nil {
                errorObservedExpectation.fulfill()
            }
        }

        viewModel.loadPosts()
        waitForExpectations(timeout: 0.1)

        // then
        XCTAssertFalse(viewModel.isLoading.value)
    }

    func testSynchronisationFailures() {
        let error = NSError.some

        let failureList: [(posts: Bool, users: Bool, comments: Bool)] = [
            (false, false, false),
            (false, false, true),
            (false, true, true),
            (true, false, false),
            (true, true, false),
            (false, true, false)
        ]

        for failure in failureList {
            let postService = failure.posts
                ? PostServiceMock(fetchedPosts: posts, storedPosts: nil, error: nil)
                : PostServiceMock(fetchedPosts: nil, storedPosts: nil, error: error)
            let userService = failure.users
                ? UserServiceMock(fetchedUsers: users, error: nil)
                : UserServiceMock(fetchedUsers: nil, error: error)
            let commentService = failure.comments
                ? CommentServiceMock(fetchedComments: [], error: nil)
                : CommentServiceMock(fetchedComments: nil, error: error)
            let viewModel = PostsViewModel(postService: postService, userService: userService, commentService: commentService)

            let exp = self.expectation(description: "")

            viewModel.errorMessage.signal.observeValues { (value) in
                if value != nil {
                    exp.fulfill()
                }
            }

            viewModel.loadPosts()

            waitForExpectations(timeout: 0.1) { error in
                guard error != nil else { return }
                XCTFail("failed to execute test for map \(failure)")
            }

            XCTAssertNotNil(viewModel.errorMessage.value, "failed to execute test for map \(failure)")
        }
    }

    func testDelegateMethodsCalls() {
        //given
        let postIndexToSelect = 1
        let viewModel = postViewModel(fetchedPosts: posts, storedPosts: nil, error: nil)

        let postsLoadedExpectation = expectation(description: "posts loaded")
        viewModel.posts.signal.observeValues { (posts) in
            if posts.count > 0 {
                postsLoadedExpectation.fulfill()
            }
        }

        //when
        viewModel.loadPosts()
        wait(for: [postsLoadedExpectation], timeout: 0.1)

        guard viewModel.posts.value.count > 0 else {
            XCTFail("view model not loaded")
            return
        }

        let selectedPost = viewModel.posts.value[postIndexToSelect].title
        var selectedPostTitle: String?

        let delegate = SomePostViewModelDelegate(presentPostDetailsHandler: { details in
            selectedPostTitle = details.title.value
        })

        viewModel.delegate = delegate
        viewModel.didSelect(postAt: postIndexToSelect)

        XCTAssertEqual(selectedPostTitle, selectedPost)
    }

    func testLoadDataAfterFailure() {
        //given
        let postService = PostServiceMock(fetchedPosts: nil, storedPosts: nil, error: NSError.some)
        let userService = UserServiceMock(fetchedUsers: users, error: nil)
        let commentService = CommentServiceMock(fetchedComments: [], error: nil)
        let viewModel = PostsViewModel(postService: postService, userService: userService, commentService: commentService)

        let failureExpectation = expectation(description: "failure observed")
        viewModel.errorMessage.signal.observeValues { (errorMessage) in
            if errorMessage != nil {
                failureExpectation.fulfill()
            }
        }
        viewModel.loadPosts()

        waitForExpectations(timeout: 0.1)

        guard viewModel.errorMessage.value != nil else {
            XCTFail("not failed to execute the case")
            return
        }

        postService.error = nil
        postService.fetchedPosts = posts

        //when
        let postsExpectation = expectation(description: "posts field is observed")
        viewModel.posts.signal.observeValues { value in
            if value.count > 0 {
                postsExpectation.fulfill()
            }
        }

        viewModel.loadPosts()

        waitForExpectations(timeout: 0.1)

        XCTAssertNil(viewModel.errorMessage.value)
        XCTAssertEqual(viewModel.posts.value.count, posts.count)
    }

    func testReloadData() {
        //given
        continueAfterFailure = false
        let posts0 = Array(posts[..<(posts.count / 2)])
        let posts1 = posts
        let postService = PostServiceMock(fetchedPosts: posts0, storedPosts: nil, error: nil)
        let userService = UserServiceMock(fetchedUsers: users, error: nil)
        let commentService = CommentServiceMock(fetchedComments: [], error: nil)
        let viewModel = PostsViewModel(postService: postService, userService: userService, commentService: commentService)

        load(viewModel: viewModel, expectedFullfillments: 2)

        XCTAssertEqual(posts0.count, viewModel.posts.value.count)

        postService.fetchedPosts = posts1
        load(viewModel: viewModel, expectedFullfillments: 1)

        XCTAssertEqual(posts1.count, viewModel.posts.value.count)
    }

    // MARK: - helpers

    func postViewModel(fetchedPosts: [PostModel]?, storedPosts: [PostModel]?, error: Error?) -> PostsViewModel {
        let postService = PostServiceMock(fetchedPosts: fetchedPosts, storedPosts: storedPosts, error: error)
        let userService = UserServiceMock(fetchedUsers: users, error: nil)
        let commentService = CommentServiceMock(fetchedComments: [], error: nil)
        let viewModel = PostsViewModel(postService: postService, userService: userService, commentService: commentService)
        return viewModel
    }

    private func load(viewModel: PostsViewModel, expectedFullfillments: Int) {
        let loadedExpectation = expectation(description: "posts observed")
        loadedExpectation.expectedFulfillmentCount = expectedFullfillments
        loadedExpectation.assertForOverFulfill = false

        viewModel.posts.signal.observeValues { (value) in
            loadedExpectation.fulfill()
        }
        viewModel.loadPosts()

        wait(for: [loadedExpectation], timeout: 0.1)
    }
}

private class SomePostViewModelDelegate: PostsViewModelInterfaceDelegate {
    let presentPostDetailsHandler: (PostDetailsViewModelInterface) -> ()

    init(presentPostDetailsHandler: @escaping (PostDetailsViewModelInterface) -> ()) {
        self.presentPostDetailsHandler = presentPostDetailsHandler
    }

    func presentPostDetails(with viewModel: PostDetailsViewModelInterface) {
        presentPostDetailsHandler(viewModel)
    }
}
