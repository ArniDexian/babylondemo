//
//  PostServiceMock.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 22/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation
import ReactiveSwift
@testable import BabylonDemo

class PostServiceMock: PostServiceInterface {
    var fetchedPosts: [PostModel]?
    var storedPosts: [PostModel]?
    var error: Error?

    init(fetchedPosts: [PostModel]?, storedPosts: [PostModel]?, error: Error?) {
        self.fetchedPosts = fetchedPosts
        self.storedPosts = storedPosts
        self.error = error
    }

    func getPosts(fetchData: Bool) -> SignalProducer<[PostModel], Error> {
        return SignalProducer { (observer, _) in
            if fetchData, let error = self.error {
                observer.send(error: error)
            } else if fetchData {
                observer.send(value: self.fetchedPosts ?? [])
            } else {
                observer.send(value: self.storedPosts ?? [])
            }
            
            observer.sendCompleted()
        }
    }
}
