//
//  CommentServiceMock.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 22/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation
import ReactiveSwift
@testable import BabylonDemo

class CommentServiceMock: CommentServiceInterface {
    var fetchedComments: [CommentModel]?
    var error: Error?

    init(fetchedComments: [CommentModel]?, error: Error?) {
        self.fetchedComments = fetchedComments
        self.error = error
    }

    func fetchComments() -> SignalProducer<[CommentModel], Error> {
        return SignalProducer { (observer, _) in
            if let error = self.error {
                observer.send(error: error)
            } else {
                observer.send(value: self.fetchedComments ?? [])
            }
            observer.sendCompleted()
        }
    }
}
