//
//  UserServiceMock.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 22/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation
import ReactiveSwift
@testable import BabylonDemo

class UserServiceMock: UserServiceInterface {
    var fetchedUsers: [UserModel]?
    var error: Error?

    init(fetchedUsers: [UserModel]?, error: Error?) {
        self.fetchedUsers = fetchedUsers
        self.error = error
    }

    func fetchUsers() -> SignalProducer<[UserModel], Error> {
        return SignalProducer { (observer, _) in
            if let error = self.error {
                observer.send(error: error)
            } else {
                observer.send(value: self.fetchedUsers ?? [])
            }
            observer.sendCompleted()
        }
    }
}
