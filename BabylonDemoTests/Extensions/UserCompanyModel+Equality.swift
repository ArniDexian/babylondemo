//
//  UserCompany+Equality.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 09/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation
@testable import BabylonDemo

func ==(lhs: UserCompanyResponseModel?, rhs: UserCompanyModel?) -> Bool {
    return rhs == lhs
}

func ==(lhs: UserCompanyModel?, rhs: UserCompanyResponseModel?) -> Bool {
    guard let lhs = lhs, let rhs = rhs else { return false }
    return lhs.bs == rhs.bs
        && lhs.catchPhrase == rhs.catchPhrase
        && lhs.name == rhs.name
}
