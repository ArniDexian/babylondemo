//
//  UserModel+Equality.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 09/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation
@testable import BabylonDemo

func ==(lhs: UserResponseModel?, rhs: UserModel?) -> Bool {
    return rhs == lhs
}

func ==(lhs: UserModel?, rhs: UserResponseModel?) -> Bool {
    guard let lhs = lhs, let rhs = rhs else { return false }
    return lhs.id == String(rhs.id)
        && lhs.name == rhs.name
        && lhs.email == rhs.email
        && lhs.phone == rhs.phone
        && lhs.username == rhs.username
        && lhs.website == rhs.website
        && lhs.addressModel == rhs.address
        && lhs.companyModel == rhs.company
}

