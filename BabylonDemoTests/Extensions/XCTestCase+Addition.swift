//
//  XCTestCase+Addition.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 20/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest

extension XCTestCase {
    func abort(if condition: @autoclosure () -> Bool,
               _ message: String? = nil,
               file: StaticString = #file,
               line: UInt = #line) {
        guard condition() else {
            return
        }

        abort(message, file: file, line: line)
    }

    func abort(_ message: String? = nil,
               file: StaticString = #file,
               line: UInt = #line) -> Never {
        continueAfterFailure = false
        XCTFail(message ?? "aborted", file: file, line: line)
        fatalError("never reached")
    }
}
