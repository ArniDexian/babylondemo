//
//  NSPersistentContainer+Extension.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 02/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import CoreData

extension NSPersistentContainer {
    convenience init(name modelName: String, storeType: String = NSInMemoryStoreType) {
        self.init(name: modelName)

        let description = NSPersistentStoreDescription()
        description.type = storeType

        if description.type == NSSQLiteStoreType || description.type == NSBinaryStoreType {
            // for persistence on local storage we need to set url
            description.url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                .first?.appendingPathComponent("Database")
        }
        persistentStoreDescriptions = [description]
    }
}
