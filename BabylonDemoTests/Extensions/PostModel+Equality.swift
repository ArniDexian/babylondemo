//
//  PostModel+Equality.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 05/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import CoreData
@testable import BabylonDemo

func ==(lhs: PostResponseModel?, rhs: PostModel?) -> Bool {
    return rhs == lhs
}

func ==(lhs: PostModel?, rhs: PostResponseModel?) -> Bool {
    guard let lhs = lhs, let rhs = rhs else { return false }
    return lhs.id == String(rhs.id)
        && lhs.title == rhs.title
        && lhs.body == rhs.body
        && lhs.userModel.id == String(rhs.userId)
}
