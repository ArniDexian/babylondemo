//
//  Bundle+Extension.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 02/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

extension Bundle {
    static var test: Bundle {
        return Bundle(identifier: "com.arnidexian.BabylonDemoTests")!
    }
}
