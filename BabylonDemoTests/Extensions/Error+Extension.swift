//
//  Error+Extension.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 22/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

extension Error {
    static var some: Error {
        return NSError(domain: "tests", code: -1, userInfo: ["source" : "\(#file):\(#line)"])
    }
}
