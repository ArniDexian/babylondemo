//
//  CommentModel+Equality.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 20/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import CoreData
@testable import BabylonDemo

func ==(lhs: CommentResponseModel?, rhs: CommentModel?) -> Bool {
    return rhs == lhs
}

func ==(lhs: CommentModel?, rhs: CommentResponseModel?) -> Bool {
    guard let lhs = lhs, let rhs = rhs else { return false }
    return lhs.id == String(rhs.id)
        && lhs.name == rhs.name
        && lhs.body == rhs.body
        && lhs.email == rhs.email
        && lhs.postModel?.id == String(rhs.postId)
}
