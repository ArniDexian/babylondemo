//
//  AddressModel+Equality.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 09/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation
@testable import BabylonDemo

func ==(lhs: AddressResponseModel?, rhs: AddressModel?) -> Bool {
    return rhs == lhs
}

func ==(lhs: AddressModel?, rhs: AddressResponseModel?) -> Bool {
    guard let lhs = lhs, let rhs = rhs else { return false }
    return lhs.suite == rhs.suite
        && lhs.city == rhs.city
        && lhs.street == rhs.street
        && lhs.zipCode == rhs.zipcode
        && lhs.latitude?.doubleValue == rhs.geo?.latitude.double
        && lhs.longitude?.doubleValue == rhs.geo?.longitude.double
}


