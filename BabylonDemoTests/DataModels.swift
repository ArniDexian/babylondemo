//
//  DataModels.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 22/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation
@testable import BabylonDemo

struct PostTestModel: PostModel {
    var id: String
    var title: String?
    var body: String?
    var commentModels: [CommentModel]?
    var userModel: UserModel
}

struct CommentTestModel: CommentModel {
    var body: String?
    var email: String?
    var id: String?
    var name: String?
    var postModel: PostModel?
}

struct AddressTestModel: AddressModel {
    var suite: String?
    var city: String?
    var street: String?
    var zipCode: String?
    var latitude: NSNumber?
    var longitude: NSNumber?
    var userModel: UserModel?
}

struct UserTestModel: UserModel {
    var id: String
    var name: String?
    var email: String?
    var phone: String?
    var username: String?
    var website: String?
    var addressModel: AddressModel?
    var companyModel: UserCompanyModel?
    var postModels: [PostModel]?
}

struct UserCompanyTestModel: UserCompanyModel {
    var bs: String?
    var catchPhrase: String?
    var name: String?
    var userModel: UserModel?
}
