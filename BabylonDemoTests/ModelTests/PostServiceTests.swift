//
//  PostServiceTests.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 05/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
import CoreData
import ReactiveSwift
@testable import BabylonDemo

class PostServiceTests: XCTestCase {

    var dataStorage: DataStorage!
    var testingPosts: [PostResponseModel] {
        return [
            PostResponseModel(userId: 1, id: 1, title: "post 1", body: "post 1 body"),
            PostResponseModel(userId: 1, id: 2, title: "post 2", body: "post 2 body"),
            PostResponseModel(userId: 2, id: 3, title: "post 3", body: "post 3 body"),
            PostResponseModel(userId: 2, id: 4, title: "post 4", body: "post 4 body"),
            PostResponseModel(userId: 2, id: 5, title: "post 5", body: "post 5 body"),
            PostResponseModel(userId: 3, id: 6, title: "post 6", body: "post 6 body"),
            PostResponseModel(userId: 4, id: 7, title: "post 7", body: "post 7 body"),
        ]
    }

    override func setUp() {
        let container = NSPersistentContainer(name: "BabylonDemo", storeType: NSInMemoryStoreType)
        dataStorage = DataStorage(persistentContainer: container)
    }

    override func tearDown() {
        dataStorage.viewContext.reset()
        dataStorage = nil
    }

    func testFetchPostsSuccessfully() {
        let expectedPosts = testingPosts
        let httpService = PostHTTPServiceMock(posts: expectedPosts)
        let service = PostService(dataStorage: dataStorage, httpService: httpService)

        let fetchExpectation = expectation(description: "\(#function) service.getPosts completion block is called")

        service.getPosts(fetchData: true).startWithResult { (result) in
            defer { fetchExpectation.fulfill() }
            
            switch result {
            case let .success(postModels):
                guard postModels.count == expectedPosts.count else {
                    XCTFail("\(#function) expected posts count != real response count")
                    return
                }

                for postModel in postModels {
                    let exist = expectedPosts.contains(where: { (expPost) -> Bool in
                        return postModel == expPost
                    })
                    XCTAssertTrue(exist, "\(#function) failed to find response model \(postModel) in expected models list")
                }
            case let .failure(error):
                XCTFail("\(#function) falied with error \(error)")
            }
        }

        waitForExpectations(timeout: 1)
    }

    func testFetchPostsFailure() {
        let httpError = NSError(domain: NSURLErrorDomain, code: NSURLErrorTimedOut, userInfo: nil)
        let httpService = PostHTTPServiceMock(error: httpError)
        let service = PostService(dataStorage: dataStorage, httpService: httpService)

        let fetchExpectation = expectation(description: "\(#function) service.getPosts completion block is called")

        service.getPosts(fetchData: true).startWithResult { (result) in
            switch result {
            case .success(_):
                XCTFail("\(#function) got success")
            case .failure(_):
                break
            }

            fetchExpectation.fulfill()
        }

        waitForExpectations(timeout: 1)
    }

    func testGetLocalPosts() {
        let expectedPosts = testingPosts
        let httpService = PostHTTPServiceMock(posts: expectedPosts)
        let service = PostService(dataStorage: dataStorage, httpService: httpService)

        let exp = expectation(description: "\(#function)")

        //first create posts then fetch them
        service.getPosts(fetchData: true).then(service.getPosts(fetchData: false)).startWithResult { (result) in

            switch result {
            case let .success(postModels):
                if postModels.count == expectedPosts.count {
                    for postModel in postModels {
                        let exist = expectedPosts.contains(where: { (expPost) -> Bool in
                            return postModel == expPost
                        })
                        XCTAssertTrue(exist, "\(#function) failed to find response model \(postModel) in expected models list")
                    }
                } else {
                    XCTFail("\(#function) expected posts count != real response count")
                }

            case let .failure(error):
                XCTFail("\(#function) failed to get posts, error: \(error)")
            }

            exp.fulfill()
        }

        waitForExpectations(timeout: 1)
    }
}
