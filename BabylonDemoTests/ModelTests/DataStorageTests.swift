//
//  DataStorageTests.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 02/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
import CoreData
@testable import BabylonDemo

class DataStorageTests: XCTestCase {

    var dataStorage: DataStorage!

    override func setUp() {
        let container = NSPersistentContainer(name: "BabylonDemo", storeType: NSInMemoryStoreType)
        dataStorage = DataStorage(persistentContainer: container)
    }

    override func tearDown() {
        dataStorage.viewContext.reset()
        dataStorage = nil
    }

    func testInitialization() {
        XCTAssertNotNil(dataStorage)
    }

    func testCreateAndCount() {
        let initCount = dataStorage.count(User.self)

        let exp = expectation(description: "test that one user created")
        dataStorage.create(type: User.self, updateHandler: { (user) in
            user.id = "12"
            user.name = "Sasha"
            user.username = "sasha84"
        }) { (result) in
            XCTAssertNotNil(result)
            exp.fulfill()
        }

        waitForExpectations(timeout: 1)

        let curCount = dataStorage.count(User.self)

        XCTAssertNotNil(initCount)
        XCTAssertNotNil(curCount)
        XCTAssertEqual(curCount ?? 0, (initCount ?? 0) + 1)
    }

    func testRead() {
        let exp = expectation(description: "test that one user created")
        dataStorage.create(type: User.self, updateHandler: { (user) in
            user.id = "12"
            user.name = "Sasha"
            user.username = "sasha84"
        }) { (result) in
            XCTAssertNotNil(result)
            exp.fulfill()
        }

        waitForExpectations(timeout: 1)

        let users = dataStorage.get(User.self)

        XCTAssertNotNil(users)
        XCTAssertEqual(users?.count, 1)
        XCTAssertEqual(users?.first?.id, "12")
    }

    func testSearch() {
        let users = ["User1", "User2", "User3", "User5"]

        users.forEach { field in
            let exp = expectation(description: "test that one user created")
            dataStorage.create(type: User.self, updateHandler: { (user) in
                user.id = field
                user.name = field
                user.username = field
            }) { (result) in
                XCTAssertNotNil(result)
                exp.fulfill()
            }
            waitForExpectations(timeout: 1)
        }

        let testId1 = "User3"
        let test1 = dataStorage.get(User.self, with: NSPredicate(format: "id == %@", testId1))

        XCTAssertNotNil(test1)
        XCTAssertEqual(test1?.count, 1)
        XCTAssertEqual(test1?.first?.id, testId1)

        let testId2 = "User4"
        let test2 = dataStorage.get(User.self, with: NSPredicate(format: "id == %@", testId2))

        XCTAssertNotNil(test2)
        XCTAssertEqual(test2?.count, 0)
    }

    func testSort() {
        let users = ["User1", "User2", "User3", "User5"]

        users.reversed().forEach { field in
            let exp = expectation(description: "test that user created")
            dataStorage.create(type: User.self, updateHandler: { (user) in
                user.id = field
                user.name = field
                user.username = field
            }) { (result) in
                XCTAssertNotNil(result)
                exp.fulfill()
            }
            waitForExpectations(timeout: 1)
        }

        let sortedMappedResult = dataStorage.get(User.self, sort: [NSSortDescriptor(key: "id", ascending: true)])?.map { $0.id } ?? []

        XCTAssertEqual(sortedMappedResult, users)
    }
}
