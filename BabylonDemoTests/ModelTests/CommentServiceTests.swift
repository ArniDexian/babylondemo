//
//  CommentServiceTests.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 20/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
import CoreData
import ReactiveSwift
@testable import BabylonDemo

class CommentServiceTests: XCTestCase {

    var dataStorage: DataStorage!

    var testingPosts: [PostResponseModel] {
        return (0..<3).map { i in
            return PostResponseModel(userId: 1, id: Int64(i), title: "post \(i)", body: "post \(i) body")
        }
    }

    var testingComments: [CommentResponseModel] {
        return (0..<10).map { i in
            CommentResponseModel(postId: Int64.random(in: 1..<3), id: Int64(i), name: "name \(i)", email: "email\(i)", body: "body \(i)")
        }
    }

    override func setUp() {
        let container = NSPersistentContainer(name: "BabylonDemo", storeType: NSInMemoryStoreType)
        dataStorage = DataStorage(persistentContainer: container)

        //preload with posts
        let postService = PostService(dataStorage: dataStorage, httpService: PostHTTPServiceMock(posts: testingPosts))

        let postsFetchedExpectation = expectation(description: "posts loaded")
        var isLoaded = false

        postService.getPosts(fetchData: true).startWithResult { (result) in
            XCTAssertNil(result.error, "can not prefetch posts, post service or http mock is corrupted")
            isLoaded = result.error == nil
            postsFetchedExpectation.fulfill()
        }

        waitForExpectations(timeout: 0.1)

        abort(if: !isLoaded, "failed to setUp as posts failed to preload")
    }

    override func tearDown() {
        dataStorage.viewContext.reset()
        dataStorage = nil
    }

    func testFetchCommentsSuccessfully() {
        let expectedComments = testingComments
        let httpService = CommentHTTPServiceMock(comments: expectedComments)
        let service = CommentService(dataStorage: dataStorage, httpService: httpService)

        let fetchExpectation = expectation(description: "\(#function) service.getComments completion block is called")

        service.fetchComments().startWithResult { (result) in
            defer { fetchExpectation.fulfill() }

            switch result {
            case let .success(commentModels):
                guard commentModels.count == expectedComments.count else {
                    XCTFail("\(#function) expected comments count != real response count")
                    return
                }

                for commentModel in commentModels {
                    let exist = expectedComments.contains(where: { (expComment) -> Bool in
                        return commentModel == expComment
                    })
                    XCTAssertTrue(exist, "\(#function) failed to find response model \(commentModel) in expected models list")
                }
            case let .failure(error):
                XCTFail("\(#function) falied with error \(error)")
            }
        }

        waitForExpectations(timeout: 1)
    }

    func testFetchCommentsWithUnknownPost() {
        var expectedComments = testingComments
        let failedCommentId: Int64 = 404
        expectedComments.append(CommentResponseModel(postId: 404, id: failedCommentId, name: "name 404", email: "email", body: "body 404"))
        let httpService = CommentHTTPServiceMock(comments: expectedComments)
        let service = CommentService(dataStorage: dataStorage, httpService: httpService)

        let fetchExpectation = expectation(description: "\(#function) service.getComments completion block is called")

        service.fetchComments().startWithResult { (result) in
            defer { fetchExpectation.fulfill() }

            switch result {
            case let .success(commentModels):
                XCTAssertEqual(commentModels.count, expectedComments.count - 1)
                XCTAssertFalse(commentModels.contains(where: {$0.id == String(failedCommentId)}))
            case let .failure(error):
                XCTFail("\(#function) falied with error \(error)")
            }
        }

        waitForExpectations(timeout: 1)
    }

    func testFetchCommentsFailure() {
        let httpError = NSError(domain: NSURLErrorDomain, code: NSURLErrorTimedOut, userInfo: nil)
        let httpService = CommentHTTPServiceMock(error: httpError)
        let service = CommentService(dataStorage: dataStorage, httpService: httpService)

        let fetchExpectation = expectation(description: "\(#function) service.getComments completion block is called")

        service.fetchComments().startWithResult { (result) in
            switch result {
            case .success(_):
                XCTFail("\(#function) got success")
            case .failure(_):
                break
            }

            fetchExpectation.fulfill()
        }

        waitForExpectations(timeout: 1)
    }
}


