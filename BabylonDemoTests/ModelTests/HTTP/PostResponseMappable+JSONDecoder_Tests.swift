//
//  PostResponseMappable+JSONDecoder_Tests.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 05/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
@testable import BabylonDemo

class PostResponseMappable_JSONDecoder_Tests: XCTestCase {

    var mapper: PostResponseMappable!

    override func setUp() {
        mapper = JSONDecoder()
    }

    override func tearDown() {
        mapper = nil
    }

    func testParsingSuccess() {
        guard let dataUrl = Bundle.test.url(forResource: "api_post_response", withExtension: "txt"),
            let responseData = try? Data(contentsOf: dataUrl) else {
                XCTFail("\(#function) failed to load test data")
                return
        }

        let expectedCount = 100

        do {
            let mapResult = try mapper.map(data: responseData)

            XCTAssertEqual(mapResult.count, expectedCount)

            for post in mapResult {
                XCTAssertGreaterThan(post.id, 0)
                XCTAssertGreaterThan(post.userId, 0)
                XCTAssertGreaterThan(post.title.count, 0)
                XCTAssertGreaterThan(post.body.count, 0)
            }
        } catch {
            XCTFail("\(#function) failed to map response, error \(error)")
        }
    }

    func testParsingFailed() {
        let incorrectJSON = """
        [{
            "userID": 1,
            "postID": 1,
            "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
            "description": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
          }]
        """
        let jsonData = incorrectJSON.data(using: .utf8)!

        do {
            _ = try mapper.map(data: jsonData)
            XCTFail("\(#function) shouldn't reach this block with incorrect data")
        } catch { }
    }
}
