//
//  PostHTTPServiceTests.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 08/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
import ReactiveSwift
@testable import BabylonDemo

class PostHTTPServiceTests: XCTestCase {
    func testResponseReceived() {
        let responseText = "response_code"
        let expectedResponse = [PostResponseModel(userId: 1, id: 1, title: "1", body: "1")]

        let url = URL(string: "http://service.com/posts")!
        let responseData = responseText.data(using: .utf8)!
        let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
        let session = URLSessionMock(data: responseData, response: response, error: nil)

        let failedError = NSError(domain: "test", code: 1, userInfo: nil)

        let mapper = PostResponseMapperMock { data throws in
            if data.utf8String == responseText {
                return expectedResponse
            } else {
                throw failedError
            }
        }

        let service = PostHTTPService(session: session, responseMapper: mapper)

        let responseExpectation = expectation(description: "\(#function) service response received")
        let completedExpectation = expectation(description: "\(#function) signal producer is completed")
        let disposedExpectation = expectation(description: "\(#function) signal producer is disposed")

        service.requestPosts().on(completed: {
            completedExpectation.fulfill()
        }).on(disposed: {
            disposedExpectation.fulfill()
        }).startWithResult { (result) in
            defer { responseExpectation.fulfill() }

            switch result {
            case let .success(models):
                XCTAssertEqual(models.first?.id, expectedResponse.first?.id)
            case let .failure(error):
                XCTFail("\(#function) failed with error \(error)")
            }
        }

        waitForExpectations(timeout: 1)
    }

    func testNoDataResponseReceived() {
        let url = URL(string: "http://service.com/posts")!
        let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
        let session = URLSessionMock(data: nil, response: response, error: nil)

        let failedError = NSError(domain: "test", code: 1, userInfo: nil)

        let mapper = PostResponseMapperMock { data throws in
            throw failedError //shouldn't be called
        }

        let service = PostHTTPService(session: session, responseMapper: mapper)

        let responseExpectation = expectation(description: "\(#function) service response received")
        let terminatedExpectation = expectation(description: "\(#function) signal producer is completed")
        let disposedExpectation = expectation(description: "\(#function) signal producer is disposed")

        service.requestPosts().on(terminated: {
            terminatedExpectation.fulfill()
        }).on(disposed: {
            disposedExpectation.fulfill()
        }).startWithResult { (result) in
            defer { responseExpectation.fulfill() }

            switch result {
            case .success:
                XCTFail("\(#function) failure is expected")
            case let .failure(error):
                guard case MapError.mappingFailed = error else {
                    XCTFail("\(#function) expected MapError.mappingFailed but got \(error)")
                    return
                }
            }
        }

        waitForExpectations(timeout: 1)
    }

    func testMappingFailure() {
        let url = URL(string: "http://service.com/posts")!
        let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
        let session = URLSessionMock(data: Data(), response: response, error: nil)

        let expectedError = NSError(domain: "test", code: 1, userInfo: nil)

        let mapper = PostResponseMapperMock { data throws in
            throw expectedError
        }

        let service = PostHTTPService(session: session, responseMapper: mapper)

        let responseExpectation = expectation(description: "\(#function) service response received")
        let terminatedExpectation = expectation(description: "\(#function) signal producer is completed")
        let disposedExpectation = expectation(description: "\(#function) signal producer is disposed")

        service.requestPosts().on(terminated: {
            terminatedExpectation.fulfill()
        }).on(disposed: {
            disposedExpectation.fulfill()
        }).startWithResult { (result) in
            defer { responseExpectation.fulfill() }

            switch result {
            case .success:
                XCTFail("\(#function) failure is expected")
            case let .failure(error):
                guard error as NSError == expectedError else {
                    XCTFail("\(#function) expected for \(expectedError) but got \(error)")
                    return
                }
            }
        }

        waitForExpectations(timeout: 1)
    }

    func testResponseWithStatusCode400() {
        let url = URL(string: "http://service.com/posts")!
        let response = HTTPURLResponse(url: url, statusCode: 400, httpVersion: nil, headerFields: nil)
        let session = URLSessionMock(data: nil, response: response, error: nil)

        let mapper = PostResponseMapperMock { data throws in
            throw NSError(domain: "test", code: 1, userInfo: nil)
        }

        let service = PostHTTPService(session: session, responseMapper: mapper)

        let responseExpectation = expectation(description: "\(#function) service response received")
        let terminatedExpectation = expectation(description: "\(#function) signal producer is completed")
        let disposedExpectation = expectation(description: "\(#function) signal producer is disposed")

        service.requestPosts().on(terminated: {
            terminatedExpectation.fulfill()
        }).on(disposed: {
            disposedExpectation.fulfill()
        }).startWithResult { (result) in
            defer { responseExpectation.fulfill() }

            switch result {
            case .success:
                XCTFail("\(#function) failure is expected")
            case let .failure(error):
                guard case HTTPRequestError.requestFailed = error else {
                    XCTFail("\(#function) expected for HTTPRequestError.requestFailed but got \(error)")
                    return
                }
            }
        }

        waitForExpectations(timeout: 1)
    }
}
