//
//  CommentHTTPService.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 20/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
import ReactiveSwift
@testable import BabylonDemo

class CommentHTTPServiceTests: XCTestCase {
    let testComments = [CommentResponseModel(postId: 1, id: 1, name: "name1", email: "email1@gmail.com", body: "body1"),
                        CommentResponseModel(postId: 1, id: 2, name: "name2", email: "email2@gmail.com", body: "body2"),
                        CommentResponseModel(postId: 2, id: 3, name: "name3", email: "email3@gmail.com", body: "body3")]

    func testResponseReceived() {
        let responseText = "response_code"
        let expectedResponse = testComments

        let url = URL(string: "http://service.com/comments")!
        let responseData = responseText.data(using: .utf8)!
        let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
        let session = URLSessionMock(data: responseData, response: response, error: nil)

        let failedError: Error = NSError(domain: "test", code: 1, userInfo: nil)

        let mapper = CommentResponseMapperMock { data throws in
            if data.utf8String == responseText {
                return expectedResponse
            } else {
                throw failedError
            }
        }

        let service = CommentHTTPService(session: session, responseMapper: mapper)

        let responseExpectation = expectation(description: "\(#function) service response received")
        let completedExpectation = expectation(description: "\(#function) signal producer is completed")
        let disposedExpectation = expectation(description: "\(#function) signal producer is disposed")

        service.requestComments().on(completed: {
            completedExpectation.fulfill()
        }).on(disposed: {
            disposedExpectation.fulfill()
        }).startWithResult { (result) in
            defer { responseExpectation.fulfill() }

            switch result {
            case let .success(models):
                XCTAssertEqual(models.first?.id, expectedResponse.first?.id)
            case let .failure(error):
                XCTFail("\(#function) failed with error \(error)")
            }
        }

        waitForExpectations(timeout: 1)
    }

    func testNoDataResponseReceived() {
        let url = URL(string: "http://service.com/comments")!
        let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
        let session = URLSessionMock(data: nil, response: response, error: nil)

        let failedError = NSError(domain: "test", code: 1, userInfo: nil)

        let mapper = CommentResponseMapperMock { data throws in
            throw failedError //shouldn't be called
        }

        let service = CommentHTTPService(session: session, responseMapper: mapper)

        let responseExpectation = expectation(description: "\(#function) service response received")
        let terminatedExpectation = expectation(description: "\(#function) signal producer is completed")
        let disposedExpectation = expectation(description: "\(#function) signal producer is disposed")

        service.requestComments().on(terminated: {
            terminatedExpectation.fulfill()
        }).on(disposed: {
            disposedExpectation.fulfill()
        }).startWithResult { (result) in
            defer { responseExpectation.fulfill() }

            switch result {
            case .success:
                XCTFail("\(#function) failure is expected")
            case let .failure(error):
                guard case MapError.mappingFailed = error else {
                    XCTFail("\(#function) expected MapError.mappingFailed but got \(error)")
                    return
                }
            }
        }

        waitForExpectations(timeout: 1)
    }

    func testMappingFailure() {
        let url = URL(string: "http://service.com/comments")!
        let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
        let session = URLSessionMock(data: Data(), response: response, error: nil)

        let expectedError = NSError(domain: "test", code: 1, userInfo: nil)

        let mapper = CommentResponseMapperMock { data throws in
            throw expectedError
        }

        let service = CommentHTTPService(session: session, responseMapper: mapper)

        let responseExpectation = expectation(description: "\(#function) service response received")
        let terminatedExpectation = expectation(description: "\(#function) signal producer is completed")
        let disposedExpectation = expectation(description: "\(#function) signal producer is disposed")

        service.requestComments().on(terminated: {
            terminatedExpectation.fulfill()
        }).on(disposed: {
            disposedExpectation.fulfill()
        }).startWithResult { (result) in
            defer { responseExpectation.fulfill() }

            switch result {
            case .success:
                XCTFail("\(#function) failure is expected")
            case let .failure(error):
                guard error as NSError == expectedError else {
                    XCTFail("\(#function) expected for \(expectedError) but got \(error)")
                    return
                }
            }
        }

        waitForExpectations(timeout: 1)
    }

    func testResponseWithStatusCode400() {
        let url = URL(string: "http://service.com/comments")!
        let response = HTTPURLResponse(url: url, statusCode: 400, httpVersion: nil, headerFields: nil)
        let session = URLSessionMock(data: nil, response: response, error: nil)

        let mapper = CommentResponseMapperMock { data throws in
            throw NSError(domain: "test", code: 1, userInfo: nil)
        }

        let service = CommentHTTPService(session: session, responseMapper: mapper)

        let responseExpectation = expectation(description: "\(#function) service response received")
        let terminatedExpectation = expectation(description: "\(#function) signal producer is completed")
        let disposedExpectation = expectation(description: "\(#function) signal producer is disposed")

        service.requestComments().on(terminated: {
            terminatedExpectation.fulfill()
        }).on(disposed: {
            disposedExpectation.fulfill()
        }).startWithResult { (result) in
            defer { responseExpectation.fulfill() }

            switch result {
            case .success:
                XCTFail("\(#function) failure is expected")
            case let .failure(error):
                guard case HTTPRequestError.requestFailed = error else {
                    XCTFail("\(#function) expected for HTTPRequestError.requestFailed but got \(error)")
                    return
                }
            }
        }

        waitForExpectations(timeout: 1)
    }
}
