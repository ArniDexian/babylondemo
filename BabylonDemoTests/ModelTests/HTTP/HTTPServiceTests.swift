//
//  HTTPServiceTests.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 03/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
import ReactiveSwift
@testable import BabylonDemo

class HTTPServiceTests: XCTestCase {

    func testPerformGetRequestSuccess() {
        //given
        let expectedResponseString = "Hello world!"
        let responseData = expectedResponseString.data(using: .utf8)
        let url: URL = URL(string: "http://googleservice.com")!

        let httpResponse = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!
        let urlSessionMock = URLSessionMock(data: responseData, response: httpResponse, error: nil)
        let dataSession = HTTPService(session: urlSessionMock)
        let expectation = self.expectation(description: "\(#function) should return data correctly")

        //when
        dataSession.request(path: "", method: .get) { (result) in
            guard case let .success(data) = result,
                let udata = data,
                let decodedString = String(data: udata, encoding: .utf8) else {
                    expectation.isInverted = true
                    expectation.fulfill()
                    return
            }

            XCTAssertEqual(decodedString, expectedResponseString)

            expectation.fulfill()
        }

        //then
        waitForExpectations(timeout: 1) { (error) in
            guard let expectationError = error as? XCTestError else { return }

            XCTFail(expectationError.code == .timeoutWhileWaiting
                ? "\(#function) took to much time to perform mock response"
                : "\(#function) failed to perform mock request as there was an error or data can't be parsed, or expected data don't mutch response.data")
        }
    }

    func testPerformRequestFailureStatusCode400() {
        //given
        let url: URL = URL(string: "http://googleservice.com")!

        let httpResponse = HTTPURLResponse(url: url, statusCode: 400, httpVersion: nil, headerFields: nil)!
        let urlSessionMock = URLSessionMock(data: nil, response: httpResponse, error: nil)
        let dataSession = HTTPService(session: urlSessionMock)
        let expectation = self.expectation(description: "\(#function) should return status code 400")

        //when
        dataSession.request(path: "", method: .get) { (result) in
            guard case let .failure(error) = result,
                case let HTTPRequestError.requestFailed(response, data) = error else {
                expectation.isInverted = true
                expectation.fulfill()
                return
            }

            XCTAssertNil(data)
            XCTAssertEqual(response.statusCode, HTTPStatusCode.badRequest.rawValue)

            expectation.fulfill()
        }

        //then
        waitForExpectations(timeout: 1) { (error) in
            guard let expectationError = error as? XCTestError else { return }

            XCTFail(expectationError.code == .timeoutWhileWaiting
                ? "\(#function) took to much time to perform mock response"
                : "\(#function) failed to perform mock request as there wasn't any error, or error doesn't have correct type or status code incorrect or response data is not nil")
        }
    }

    func testPerformRequestFailureUnknownStatusCode() {
        //given
        let url: URL = URL(string: "http://googleservice.com")!

        let httpResponse = HTTPURLResponse(url: url, statusCode: 501, httpVersion: nil, headerFields: nil)!
        let urlSessionMock = URLSessionMock(data: nil, response: httpResponse, error: nil)
        let dataSession = HTTPService(session: urlSessionMock)
        let expectation = self.expectation(description: "\(#function) should return unknown status code")

        //when
        dataSession.request(path: "", method: .get) { (result) in
            guard case let .failure(error) = result,
                case let HTTPRequestError.unknownStatusCode(response, data) = error else {
                    expectation.isInverted = true
                    expectation.fulfill()
                    return
            }

            XCTAssertNil(data)
            XCTAssertNil(HTTPStatusCode(rawValue: response.statusCode))

            expectation.fulfill()
        }

        //then
        waitForExpectations(timeout: 1) { (error) in
            guard let expectationError = error as? XCTestError else { return }

            XCTFail(expectationError.code == .timeoutWhileWaiting
                ? "\(#function) took to much time to perform mock response"
                : "\(#function) failed to perform mock request as there wasn't any error, or error doesn't have correct type or status code is known or response data is not nil")
        }
    }

    func testPerformRequestFailureWithInternalError() {
        //given
        let urlSessionMock = URLSessionMock(data: nil, response: nil, error: nil)
        let dataSession = HTTPService(session: urlSessionMock)
        let expectation = self.expectation(description: "\(#function) should return internal error")

        //when
        dataSession.request(path: "", method: .get) { (result) in
            guard case let .failure(error) = result,
                case HTTPRequestError.internalError = error else {
                    expectation.isInverted = true
                    expectation.fulfill()
                    return
            }

            expectation.fulfill()
        }

        //then
        waitForExpectations(timeout: 1) { (error) in
            guard let expectationError = error as? XCTestError else { return }

            XCTFail(expectationError.code == .timeoutWhileWaiting
                ? "\(#function) took to much time to perform mock response"
                : "\(#function) failed to perform mock request as there wasn't any error, or error doesn't have correct type or error is not internalError")
        }
    }

    func testPerformRequestFailureUrlCreation() {
        //given
        let url = URL(string: "http://googleservice.com:-80")!
        let urlSessionMock = URLSessionMock(data: nil, response: nil, error: nil)
        let dataSession = HTTPService(endpoint: .custom(url), session: urlSessionMock)
        let expectation = self.expectation(description: "\(#function) should return urlCreation error")

        //when
        dataSession.request(path: "", method: .get) { (result) in
            guard case let .failure(error) = result,
                case HTTPRequestError.urlCreation = error else {
                    expectation.isInverted = true
                    expectation.fulfill()
                    return
            }

            expectation.fulfill()
        }

        //then
        waitForExpectations(timeout: 1) { (error) in
            guard let expectationError = error as? XCTestError else { return }

            XCTFail(expectationError.code == .timeoutWhileWaiting
                ? "\(#function) took to much time to perform mock response"
                : "\(#function) failed to perform mock request as there wasn't any error, or error doesn't have correct type or error is not urlCreation")
        }
    }

    func testPerformRequestFailureWithSystemError() {
        //given
        let expectedError = NSError(domain: NSURLErrorDomain, code: NSURLErrorTimedOut, userInfo: nil)
        let urlSessionMock = URLSessionMock(data: nil, response: nil, error: expectedError)
        let dataSession = HTTPService(session: urlSessionMock)
        let expectation = self.expectation(description: "\(#function) should return system error")

        //when
        dataSession.request(path: "", method: .get) { (result) in
            guard case let .failure(error) = result else {
                    expectation.isInverted = true
                    expectation.fulfill()
                    return
            }

            XCTAssertEqual(error as NSError, expectedError)
            expectation.fulfill()
        }

        //then
        waitForExpectations(timeout: 1) { (error) in
            guard let expectationError = error as? XCTestError else { return }

            XCTFail(expectationError.code == .timeoutWhileWaiting
                ? "\(#function) took to much time to perform mock response"
                : "\(#function) failed to perform mock request as there wasn't any error, or error is not expected NSError")
        }
    }

    func testReactiveRequestSuccess() {
        //given
        let expectedResponseString = "Hello world!"
        let responseData = expectedResponseString.data(using: .utf8)
        let url: URL = URL(string: "http://googleservice.com")!

        let httpResponse = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!
        let urlSessionMock = URLSessionMock(data: responseData, response: httpResponse, error: nil)
        let dataSession = HTTPService(session: urlSessionMock)
        let dataExpectation = self.expectation(description: "\(#function) should return data correctly")
        let completedExpectation = self.expectation(description: "\(#function) should complete created SignalProducer")
        let disposedExpectation = self.expectation(description: "\(#function) should dispose created SignalProducer")

        //when
        dataSession.request(path: "", method: .get).on(completed: {
            completedExpectation.fulfill()
        }).on(disposed: {
            disposedExpectation.fulfill()
        }).startWithResult { (result) in
            guard case let .success(data) = result,
                let udata = data,
                let decodedString = String(data: udata, encoding: .utf8) else {
                    dataExpectation.isInverted = true
                    dataExpectation.fulfill()
                    return
            }

            XCTAssertEqual(decodedString, expectedResponseString)

            dataExpectation.fulfill()
        }

        //then
        waitForExpectations(timeout: 1) { (error) in
            guard let expectationError = error as? XCTestError else { return }

            XCTFail(expectationError.code == .timeoutWhileWaiting
                ? "\(#function) took to much time to perform mock response"
                : "\(#function) failed to perform reactive mock request as there was an error or data can't be parsed or expected data don't mutch response.data or signal  hasn't been completed or resources haven't been disposed")
        }
    }

    func testPerformReactiveRequestFailureWithSystemError() {
        //given
        let expectedError = NSError(domain: NSURLErrorDomain, code: NSURLErrorTimedOut, userInfo: nil)
        let urlSessionMock = URLSessionMock(data: nil, response: nil, error: expectedError)
        let dataSession = HTTPService(session: urlSessionMock)
        let expectation = self.expectation(description: "\(#function) should return system error")
        let terminatedExpectation = self.expectation(description: "\(#function) should terminate  created SignalProducer")
        let disposedExpectation = self.expectation(description: "\(#function) should dispose created SignalProducer")
        
        //when
        dataSession.request(path: "", method: .get).on(terminated: {
            terminatedExpectation.fulfill()
        }, disposed: {
            disposedExpectation.fulfill()
        }).startWithResult { (result) in
            guard case let .failure(error) = result else {
                expectation.isInverted = true
                expectation.fulfill()
                return
            }

            XCTAssertEqual(error as NSError, expectedError)
            expectation.fulfill()
        }

        //then
        waitForExpectations(timeout: 1) { (error) in
            guard let expectationError = error as? XCTestError else { return }

            XCTFail(expectationError.code == .timeoutWhileWaiting
                ? "\(#function) took to much time to perform mock response"
                : "\(#function) failed to perform mock request as there wasn't any error or error is not expected NSError or signal wasn't terminated/disposed")
        }
    }
}
