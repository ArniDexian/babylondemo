//
//  CommentHTTPServiceMock.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 20/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
import ReactiveSwift
@testable import BabylonDemo

struct CommentHTTPServiceMock: CommentHTTPServiceInterface {
    private let comments: [CommentResponseModel]?
    private let error: Error?

    init(comments: [CommentResponseModel]) {
        self.comments = comments
        self.error = nil
    }

    init(error: Error) {
        self.comments = nil
        self.error = error
    }

    func requestComments() -> SignalProducer<[CommentResponseModel], Error> {
        return SignalProducer { (observer, _) in
            guard let comments = self.comments else {
                observer.send(error: self.error!)
                return
            }
            observer.send(value: comments)
            observer.sendCompleted()
        }
    }
}
