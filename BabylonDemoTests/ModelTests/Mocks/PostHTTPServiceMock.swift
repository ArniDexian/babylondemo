//
//  PostHTTPServiceMock.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 05/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
import ReactiveSwift
@testable import BabylonDemo

struct PostHTTPServiceMock: PostHTTPServiceInterface {
    private let posts: [PostResponseModel]?
    private let error: Error?

    init(posts: [PostResponseModel]) {
        self.posts = posts
        self.error = nil
    }

    init(error: Error) {
        self.posts = nil
        self.error = error
    }

    func requestPosts() -> SignalProducer<[PostResponseModel], Error> {
        return SignalProducer { (observer, _) in
            guard let posts = self.posts else {
                observer.send(error: self.error!)
                return
            }
            observer.send(value: posts)
            observer.sendCompleted()
        }
    }
}
