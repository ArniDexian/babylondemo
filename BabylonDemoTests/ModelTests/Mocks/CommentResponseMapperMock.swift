//
//  CommentResponseMapperMock.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 20/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
import ReactiveSwift
@testable import BabylonDemo

struct CommentResponseMapperMock: CommentResponseMappable {
    let mapper: (Data) throws -> [CommentResponseModel]

    func map(data: Data) throws -> [CommentResponseModel] {
        return try mapper(data)
    }
}
