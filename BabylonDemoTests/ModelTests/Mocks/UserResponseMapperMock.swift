//
//  UserResponseMapperMock.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 08/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
import ReactiveSwift
@testable import BabylonDemo

struct UserResponseMapperMock: UserResponseMappable {
    let mapper: (Data) throws -> [UserResponseModel]

    func map(data: Data) throws -> [UserResponseModel] {
        return try mapper(data)
    }
}
