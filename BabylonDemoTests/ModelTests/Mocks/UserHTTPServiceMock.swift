//
//  UserHTTPServiceMock.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 09/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
import ReactiveSwift
@testable import BabylonDemo

struct UserHTTPServiceMock: UserHTTPServiceInterface {
    private let users: [UserResponseModel]?
    private let error: Error?

    init(users: [UserResponseModel]) {
        self.users = users
        self.error = nil
    }

    init(error: Error) {
        self.users = nil
        self.error = error
    }

    func requestUsers() -> SignalProducer<[UserResponseModel], Error> {
        return SignalProducer { (observer, _) in
            guard let users = self.users else {
                observer.send(error: self.error!)
                return
            }
            observer.send(value: users)
            observer.sendCompleted()
        }
    }
}
