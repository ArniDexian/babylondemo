//
//  PostResponseMapperMock.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 08/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
import ReactiveSwift
@testable import BabylonDemo

struct PostResponseMapperMock: PostResponseMappable {
    let mapper: (Data) throws -> [PostResponseModel]

    func map(data: Data) throws -> [PostResponseModel] {
        return try mapper(data)
    }
}
