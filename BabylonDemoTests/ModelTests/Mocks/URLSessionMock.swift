//
//  URLSessionMock.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 03/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation
@testable import BabylonDemo

class URLSessionMock: HTTPSession {

    var data: Data?
    var error: Error?
    var response: URLResponse?

    init(data: Data? = nil, response: URLResponse? = nil, error: Error? = nil) {
        self.data = data
        self.response = response
        self.error = error
    }

    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return URLSessionDataTaskMock {
            completionHandler(self.data, self.response, self.error)
        }
    }

}

private class URLSessionDataTaskMock: URLSessionDataTask {
    let completionHandler: () -> Void
    init(completionHandler: @escaping () -> Void) {
        self.completionHandler = completionHandler
    }

    override func resume() {
        self.completionHandler()
    }
}
