//
//  UserServiceTests.swift
//  BabylonDemoTests
//
//  Created by Arni Dexian on 08/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import XCTest
import CoreData
import ReactiveSwift
@testable import BabylonDemo

class UserServiceTests: XCTestCase {

    var dataStorage: DataStorage!
    var testingUsers: [UserResponseModel] {
        return (0..<10).map { i in
            UserResponseModel(id: i, name: "name", username: "user name", email: "user@mail.com", address: AddressResponseModel(street: "Street", suite: "suite", city: "City", zipcode: "11111", geo: GeoResponseModel(latitude: "0.1", longitude: "0.1")), phone: "+12345678", website: "http://user.com", company: UserCompanyResponseModel(name: "user company", catchPhrase: "catch phrase", bs: "bs"))
        }
    }

    override func setUp() {
        let container = NSPersistentContainer(name: "BabylonDemo", storeType: NSInMemoryStoreType)
        dataStorage = DataStorage(persistentContainer: container)
    }

    override func tearDown() {
        dataStorage.viewContext.reset()
        dataStorage = nil
    }

    func testFetchUsersSuccessfully() {
        let expectedUsers = testingUsers
        let httpService = UserHTTPServiceMock(users: expectedUsers)
        let service = UserService(dataStorage: dataStorage, httpService: httpService)

        let fetchExpectation = expectation(description: "\(#function) service.getUsers completion block is called")

        service.fetchUsers().startWithResult { (result) in
            defer { fetchExpectation.fulfill() }
            
            switch result {
            case let .success(userModels):
                guard userModels.count == expectedUsers.count else {
                    XCTFail("\(#function) expected users count != real response count")
                    return
                }

                for userModel in userModels {
                    let exist = expectedUsers.contains(where: { (expUser) -> Bool in
                        return userModel == expUser
                    })
                    XCTAssertTrue(exist, "\(#function) failed to find response model \(userModel) in expected models list")
                }
            case let .failure(error):
                XCTFail("\(#function) falied with error \(error)")
            }
        }

        waitForExpectations(timeout: 1)
    }

    func testFetchUsersFailure() {
        let httpError = NSError(domain: NSURLErrorDomain, code: NSURLErrorTimedOut, userInfo: nil)
        let httpService = UserHTTPServiceMock(error: httpError)
        let service = UserService(dataStorage: dataStorage, httpService: httpService)

        let fetchExpectation = expectation(description: "\(#function) service.getUsers completion block is called")

        service.fetchUsers().startWithResult { (result) in
            switch result {
            case .success(_):
                XCTFail("\(#function) got success")
            case .failure(_):
                break
            }

            fetchExpectation.fulfill()
        }

        waitForExpectations(timeout: 1)
    }
}
