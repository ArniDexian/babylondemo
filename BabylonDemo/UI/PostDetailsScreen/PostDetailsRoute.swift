//
//  PostDetailsRoute.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 28/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import UIKit

protocol PostDetailsRoute {
    func openPostDetails(with viewModel: PostDetailsViewModelInterface)
}

extension PostDetailsRoute where Self: UIViewController {
    func openPostDetails(with viewModel: PostDetailsViewModelInterface) {
        guard let navigationController = self.navigationController,
            let viewController = storyboard?.instantiateViewController(PostDetailsViewController.self) else {
                assertionFailure("\(#function) failed to navigate as navigationController or storyboard is missing for \(self)")
                return
        }

        viewController.viewModel = viewModel
        navigationController.pushViewController(viewController, animated: true)
    }
}
