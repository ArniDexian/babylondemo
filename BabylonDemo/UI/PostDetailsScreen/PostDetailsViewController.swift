//
//  PostDetailsViewController.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 12/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import ReactiveSwift
import ReactiveCocoa
import UIKit

final class PostDetailsViewController: UIViewController {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var userLabel: UILabel!
    @IBOutlet private weak var postTextView: UITextView!
    @IBOutlet private weak var commentsLabel: UILabel!

    var viewModel: PostDetailsViewModelInterface? {
        didSet {
            bindModel()
        }
    }

    private var disposable: CompositeDisposable!

    deinit {
        disposable?.dispose()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        bindModel()
    }

    private func setupView() {
        postTextView.textContainer.lineFragmentPadding = 0
        postTextView.contentInset = .zero
    }

    private func bindModel() {
        guard isViewLoaded, let model = viewModel else { return }

        if disposable?.isDisposed == false {
            disposable.dispose()
        }
        disposable = CompositeDisposable()

        disposable += titleLabel.reactive.text <~ model.title
        disposable += userLabel.reactive.text <~ model.userName
        disposable += postTextView.reactive.text <~ model.body
        disposable += commentsLabel.reactive.text <~ model.commentsText
    }
}
