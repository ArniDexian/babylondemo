//
//  PostDetailsViewModel.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 12/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import ReactiveSwift

// NOTE: move to separate file
protocol PostDetailsViewModelInterface {
    var title: Property<String> { get }
    var body: Property<String> { get }
    var userName: Property<String> { get }
    var commentsText: Property<String> { get }
}

final class PostDetailsViewModel: PostDetailsViewModelInterface {
    let title: Property<String>
    let body: Property<String>
    let userName: Property<String>
    let commentsText: Property<String>

    private let postModel: MutableProperty<PostModel>

    init(postModel: PostModel) {
        self.postModel = MutableProperty(postModel)

        title = Property(self.postModel.map { $0.title ?? "" } )
        body = Property(self.postModel.map { $0.body ?? "" } )
        userName = Property(self.postModel.map { $0.userModel.shortDetails } )
        commentsText = Property(self.postModel.map { String(format: "Comments: %i".localized, $0.commentModels?.count ?? 0) })
    }
}

private extension UserModel {
    var shortDetails: String {
        if let companyName = companyModel?.name {
            return String(format: "By %@ from %@".localized, displayName, companyName)
        } else {
            return String(format: "By %@".localized, displayName)
        }
    }
}
