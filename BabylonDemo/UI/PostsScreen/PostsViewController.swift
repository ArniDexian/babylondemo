//
//  PostsViewController.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 09/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

final class PostsViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private var errorContainer: UIView!
    @IBOutlet private weak var errorLabel: UILabel!

    var viewModel: PostsViewModelInterface? {
        didSet {
            bindModel()
        }
    }

    private var disposable: CompositeDisposable!

    deinit {
        disposable?.dispose()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        bindModel()
    }

    private func setupView() {
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.refreshControl = UIRefreshControl()
        tableView.tableFooterView = UIView()
    }

    private func bindModel() {
        guard isViewLoaded, let viewModel = viewModel else { return }

        if disposable?.isDisposed == false {
            disposable.dispose()
        }
        disposable = CompositeDisposable()

        // bind views
        disposable += tableView.refreshControl!.reactive.controlEvents(.valueChanged).observeValues { [weak self] _ in
            self?.viewModel?.loadPosts()
        }

        // bind model
        viewModel.delegate = self

        title = viewModel.title
        disposable += viewModel.posts.producer.observe(on: QueueScheduler.main).startWithValues { [weak self] _ in
            self?.tableView.reloadData()
        }

        disposable += viewModel.isLoading.producer.observe(on: QueueScheduler.main).startWithValues { [weak self] isLoading in
            self?.setIsLoadingIndicator(hidden: !isLoading)
        }

        disposable += viewModel.errorMessage.producer.observe(on: QueueScheduler.main).startWithValues { [weak self] errorText in
            self?.displayError(message: errorText)
        }

        if viewModel.posts.value.count == 0 {
            viewModel.loadPosts()
        }
    }

    private func setIsLoadingIndicator(hidden: Bool) {
        guard let control = tableView.refreshControl else { return }

        if hidden {
            control.endRefreshing()
        } else {
            if !control.isRefreshing {
                control.beginRefreshing()
                tableView.setContentOffset(CGPoint(x: 0, y: -control.bounds.height), animated: false)
            }
        }
    }

    private func displayError(message: String?) {
        if message == nil {
            tableView.tableHeaderView = nil
        } else {
            errorLabel.text = message
            let size = errorContainer.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
            errorContainer.bounds.size = CGSize(width: size.width.rounded(.up), height: size.height.rounded(.up))
            tableView.tableHeaderView = errorContainer
        }
    }
}

extension PostsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel?.posts.value.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else {
            assertionFailure("\(#function) viewModel is nil")
            return .init()
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath) as! PostTableViewCell
        cell.configure(with: viewModel.posts.value[indexPath.row])

        return cell
    }
}

extension PostsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel?.didSelect(postAt: indexPath.row)
    }
}

extension PostsViewController: PostsViewModelInterfaceDelegate, PostDetailsRoute {
    func presentPostDetails(with viewModel: PostDetailsViewModelInterface) {
        openPostDetails(with: viewModel)
    }
}
