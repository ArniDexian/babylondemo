//
//  PostCellViewModel.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 09/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

protocol PostCellViewModelInterface {
    var title: String { get }
    var author: String { get }
    var comments: String { get }
}

struct PostCellViewModel: PostCellViewModelInterface {
    let title: String
    let author: String
    let comments: String

    init(postModel: PostModel) {
        title = postModel.title ?? "-/-".localized
        author = postModel.userModel.displayName
        comments = "\(postModel.commentModels?.count ?? 0) 💬"
    }
}
