//
//  PostsViewModel.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 09/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import ReactiveSwift

protocol PostsViewModelInterfaceDelegate: class {
    func presentPostDetails(with viewModel: PostDetailsViewModelInterface)
}

// NOTE: move to separate file with `PostsViewModelInterfaceDelegate`
protocol PostsViewModelInterface: class {
    var title: String { get }
    var posts: Property<[PostCellViewModelInterface]> { get }
    var errorMessage: Property<String?> { get }
    var isLoading: Property<Bool> { get }

    var delegate: PostsViewModelInterfaceDelegate? { get set }

    func loadPosts()
    func didSelect(postAt index: Int)
}

final class PostsViewModel: PostsViewModelInterface {
    let title = "Posts".localized
    let posts: Property<[PostCellViewModelInterface]>
    let errorMessage: Property<String?>
    let isLoading: Property<Bool>

    private let postService: PostServiceInterface
    private let userService: UserServiceInterface
    private let commentService: CommentServiceInterface

    private let error = MutableProperty<Error?>(nil)
    private var postModels = MutableProperty<[PostModel]>([])
    private var isSynchronising = MutableProperty<Bool>(false)

    weak var delegate: PostsViewModelInterfaceDelegate?

    private var disposable = CompositeDisposable()

    init(postService: PostServiceInterface, userService: UserServiceInterface, commentService: CommentServiceInterface) {
        self.postService = postService
        self.userService = userService
        self.commentService = commentService
        isLoading = Property(isSynchronising)
        
        errorMessage = Property(error.map { $0 != nil ? "❌ Failed to load posts.\n Pull down to retry".localized : nil })
        posts = Property(postModels.map { $0.map { PostCellViewModel(postModel: $0) }})
    }

    deinit {
        disposable.dispose()
    }

    func loadPosts() {
        // first try to load local copy
        if postModels.value.count == 0 {
            disposable += postService.getPosts(fetchData: false).startWithResult { [weak self] result in
                self?.didLoad(with: result)
                self?.synchroniseData()
            }
        } else {
            synchroniseData()
        }
    }

    private func synchroniseData() {
        guard !isSynchronising.value else { return }

        if error.value != nil {
            error.value = nil
        }

        isSynchronising.value = true
        let fetchComments = commentService.fetchComments()

        disposable += userService.fetchUsers()
            .then(postService.getPosts(fetchData: true))
            .flatMap(.concat, { posts in
                return fetchComments.map({ _ in posts})
            }).startWithResult { [weak self](result) in
                self?.didLoad(with: result)
            }
    }

    private func didLoad(with result: Result<[PostModel], Error>) {
        if isSynchronising.value {
            isSynchronising.value = false
        }

        switch result {
        case let .success(posts):
            postModels.value = posts.sorted(by: { $0.id > $1.id})
        case let .failure(requestError):
            error.value = requestError
        }
    }

    func didSelect(postAt index: Int) {
        let post = postModels.value[index]
        let viewModel = PostDetailsViewModel(postModel: post)
        delegate?.presentPostDetails(with: viewModel)
    }
}
