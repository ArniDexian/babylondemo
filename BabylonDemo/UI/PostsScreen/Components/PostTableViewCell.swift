//
//  PostTableViewCell.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 21/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import UIKit

final class PostTableViewCell: UITableViewCell {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var authorLabel: UILabel!
    @IBOutlet private weak var commentsLabel: UILabel!
}

extension PostTableViewCell {
    func configure(with model: PostCellViewModelInterface) {
        titleLabel.text = model.title
        authorLabel.text = model.author
        commentsLabel.text = model.comments
    }
}
