//
//  AppDelegate.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 21/07/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var dataStorage: DataStorage!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        dataStorage = DataStorage(persistentContainer: NSPersistentContainer(name: "BabylonDemo", storeType: NSSQLiteStoreType))

        if window == nil {
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.makeKeyAndVisible()
        }
        
        let postService = PostService(dataStorage: dataStorage)
        let userService = UserService(dataStorage: dataStorage)
        let commentService = CommentService(dataStorage: dataStorage)

        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        let postsViewController = storyboard.instantiateViewController(PostsViewController.self)
        postsViewController.viewModel = PostsViewModel(postService: postService, userService: userService, commentService: commentService)

        window?.rootViewController = UINavigationController(rootViewController: postsViewController)

        return true
    }
}
