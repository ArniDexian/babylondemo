//
//  Comment+CoreDataProperties.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 26/07/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//
//

import Foundation
import CoreData


extension Comment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Comment> {
        return NSFetchRequest<Comment>(entityName: "Comment")
    }

    @NSManaged public var body: String?
    @NSManaged public var email: String?
    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var post: Post?

}
