//
//  UserCompany+CoreDataProperties.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 26/07/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//
//

import Foundation
import CoreData


extension UserCompany {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserCompany> {
        return NSFetchRequest<UserCompany>(entityName: "UserCompany")
    }

    @NSManaged public var bs: String?
    @NSManaged public var catchPhrase: String?
    @NSManaged public var name: String?
    @NSManaged public var user: User?

}
