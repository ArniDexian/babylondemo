//
//  Post+CoreDataProperties.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 26/07/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//
//

import Foundation
import CoreData


extension Post {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Post> {
        return NSFetchRequest<Post>(entityName: "Post")
    }

    @NSManaged public var body: String?
    @NSManaged public var id: String
    @NSManaged public var title: String?
    @NSManaged public var comments: Set<Comment>?
    @NSManaged public var user: User

}

// MARK: Generated accessors for comments
extension Post {

    @objc(addCommentsObject:)
    @NSManaged public func addToComments(_ value: Comment)

    @objc(removeCommentsObject:)
    @NSManaged public func removeFromComments(_ value: Comment)

    @objc(addComments:)
    @NSManaged public func addToComments(_ values: Set<Comment>)

    @objc(removeComments:)
    @NSManaged public func removeFromComments(_ values: Set<Comment>)

}
