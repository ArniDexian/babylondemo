//
//  Address+CoreDataProperties.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 26/07/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//
//

import Foundation
import CoreData
import CoreLocation

extension Address {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Address> {
        return NSFetchRequest<Address>(entityName: "Address")
    }

    @NSManaged public var suite: String?
    @NSManaged public var city: String?
    @NSManaged public var street: String?
    @NSManaged public var zipCode: String?
    @NSManaged public var user: User?
    @NSManaged public var latitude: NSNumber?
    @NSManaged public var longitude: NSNumber?

}
