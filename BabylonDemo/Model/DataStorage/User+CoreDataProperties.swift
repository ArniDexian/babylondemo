//
//  User+CoreDataProperties.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 26/07/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var email: String?
    @NSManaged public var id: String
    @NSManaged public var name: String?
    @NSManaged public var phone: String?
    @NSManaged public var username: String?
    @NSManaged public var website: String?
    @NSManaged public var address: Address?
    @NSManaged public var company: UserCompany?
    @NSManaged public var posts: Set<Post>?

}

// MARK: Generated accessors for posts
extension User {

    @objc(addPostsObject:)
    @NSManaged public func addToPosts(_ value: Post)

    @objc(removePostsObject:)
    @NSManaged public func removeFromPosts(_ value: Post)

    @objc(addPosts:)
    @NSManaged public func addToPosts(_ values: Set<Post>)

    @objc(removePosts:)
    @NSManaged public func removeFromPosts(_ values: Set<Post>)

}
