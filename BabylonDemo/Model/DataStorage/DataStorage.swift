//
//  DataStorage.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 21/07/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import CoreData

final class DataStorage {

    private let persistentContainer: NSPersistentContainer

    var viewContext: NSManagedObjectContext {
        persistentContainer.viewContext
    }

    init(persistentContainer: NSPersistentContainer = .default) {
        self.persistentContainer = persistentContainer
        persistentContainer.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // NOTE: not production grate solution, placed for simplicity
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
    }

    func newBackgroundContext() -> NSManagedObjectContext {
        let ctx = persistentContainer.newBackgroundContext()
        ctx.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        ctx.undoManager = nil
        return ctx
    }

    func create<T: NSManagedObject>(_ type: T.Type, context: NSManagedObjectContext) -> T? {
        NSEntityDescription.insertNewObject(forEntityName: T.entityName, into: context) as? T
    }

    func create<T: NSManagedObject>(type: T.Type, updateHandler: @escaping (T) -> Void, completion: @escaping (T?) -> Void) {
        let ctx = newBackgroundContext()

        ctx.perform {
            guard let obj = self.create(type, context: ctx) else { return }
            
            updateHandler(obj)

            do {
                try ctx.saveAndReset()

                if let refreshed = self.refreshedObject(obj) {
                    completion(refreshed)
                }
            } catch {
                // NOTE: subject to use logger/proxy object instead
                print("\(#function) failed to save created object. Error: \(error)")
                completion(nil)
            }
        }
    }

    func get<T: NSManagedObject>(_ type: T.Type, with predicate: NSPredicate? = nil, sort: [NSSortDescriptor]? = nil, in context: NSManagedObjectContext? = nil) -> [T]? {
        let request = NSFetchRequest<T>(entityName: type.entityName)
        let context = context ?? viewContext

        request.predicate = predicate
        request.sortDescriptors = sort

        var result: [T]?

        context.performAndWait {
            do {
                result = try context.fetch(request)
            } catch {
                // NOTE: subject to use logger/proxy object instead
                print("\(#function) failed to fetch request \(request). Error: \(error)")
            }
        }

        return result
    }

    func performAndSave(_ block: @escaping (NSManagedObjectContext) -> Void, completion: @escaping (Error?) -> Void) {
        let ctx = newBackgroundContext()

        ctx.perform {
            block(ctx)

            do {
                try ctx.saveAndReset()
            } catch {
                // NOTE: subject to use logger/proxy object instead
                print("\(#function) failed to save context. Error: \(error)")
                completion(nil)
            }
        }
    }

    /// Get count of entities with given type on viewContext
    func count<T: NSManagedObject>(_ type: T.Type) -> Int? {
        count(type, in: viewContext)
    }

    func count<T: NSManagedObject>(_ type: T.Type, in context: NSManagedObjectContext) -> Int? {
        var count: Int?
        context.performAndWait {
            count = try? context.count(for: NSFetchRequest<NSFetchRequestResult>(entityName: type.entityName))
        }
        return count
    }

    /// Get object from view context
    func refreshedObject<T: NSManagedObject>(_ object: T) -> T? {
        refreshedObjects([object])?.first
    }

    func refreshedObjects<T: NSManagedObject>(_ objects: [T]) -> [T]? {
        guard !objects.contains(where: { $0.objectID.isTemporaryID}) else {
            return nil
        }

        var result: [T]?

        viewContext.performAndWait {
            do {
                let refreshed = try objects.compactMap { (object) throws -> T? in
                    let viewContextObject = try viewContext.existingObject(with: object.objectID)
                    viewContext.refresh(viewContextObject, mergeChanges: true)
                    return viewContextObject as? T
                }

                result = refreshed
            } catch {
                print("\(#function) failed to get view context object")
            }
        }

        return result
    }
}

private extension NSPersistentContainer {
    static var `default`: NSPersistentContainer {
        let persistentContainer = NSPersistentContainer(name: "BabylonDemo")
        return persistentContainer
    }
}

private extension NSManagedObject {
    static var entityName: String {
        return "\(self)"
    }
}

// NOTE: candidate for separate file
extension NSManagedObjectContext {
    func saveAndReset() throws {
        if hasChanges {
            try save()
            reset()
        }
    }
}
