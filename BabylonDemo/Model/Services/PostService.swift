//
//  PostService.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 26/07/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import CoreData
import ReactiveSwift

enum PostServiceError: Error {
    case dataStorageFailure(Error?)
}

// NOTE: move to separate file under Contracts group
protocol PostServiceInterface {
    func getPosts(fetchData: Bool) -> SignalProducer<[PostModel], Error>
}

final class PostService: PostServiceInterface {
    private let dataStorage: DataStorage
    private let httpService: PostHTTPServiceInterface

    init(dataStorage: DataStorage, httpService: PostHTTPServiceInterface = PostHTTPService()) {
        self.dataStorage = dataStorage
        self.httpService = httpService
    }

    /// Load posts
    ///
    /// - Parameter fetchData: if true fetch posts from network first, otherwise load local copy
    func getPosts(fetchData: Bool) -> SignalProducer<[PostModel], Error> {
        fetchData ? fetchPosts() : loadPostsFromStorage()
    }

    private func fetchPosts() -> SignalProducer<[PostModel], Error> {
        httpService.requestPosts().flatMap(.merge) { (responseModels) in
            self.saveToStorage(response: responseModels)
        }
    }

    // NOTE: subject to re-think and reuse across all model services
    private func saveToStorage(response: [PostResponseModel]) -> SignalProducer<[PostModel], Error> {
        SignalProducer { [dataStorage](observer, lifetime) in
            let ctx = dataStorage.newBackgroundContext()

            ctx.perform {
                let posts = response.compactMap { (postResponse) -> Post? in
                    let predicate = NSPredicate(format: "id == %@", String(postResponse.id))
                    guard let post = dataStorage.get(Post.self, with: predicate, in: ctx)?.first ?? dataStorage.create(Post.self, context: ctx) else {
                        assertionFailure("failed to create post \(postResponse)")
                        return nil
                    }

                    post.map(from: postResponse, dataStorage: dataStorage)

                    return post
                }

                do {
                    try ctx.saveAndReset()

                    let updated = dataStorage.refreshedObjects(posts)
                    observer.send(value: updated ?? [])
                    observer.sendCompleted()
                } catch {
                    observer.send(error: error)
                }
            }
        }
    }

    private func loadPostsFromStorage(predicate: NSPredicate? = nil, sort: [NSSortDescriptor]? = nil) -> SignalProducer<[PostModel], Error> {
        return SignalProducer { [dataStorage](observer, lifetime) in
            guard let posts = dataStorage.get(Post.self, with: predicate, sort: sort) else {
                observer.send(error: PostServiceError.dataStorageFailure(nil))
                return
            }

            observer.send(value: posts)
            observer.sendCompleted()
        }
    }
}
