//
//  CommentModel.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 09/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

protocol CommentModel: EntityModel {
    var body: String? { get }
    var email: String? { get }
    var id: String? { get }
    var name: String? { get }
    var postModel: PostModel? { get }
}

extension Comment: CommentModel {
    var postModel: PostModel? {
        post
    }
}
