//
//  PostModel.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 09/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

protocol PostModel: EntityModel {
    var id: String { get }
    var title: String? { get }
    var body: String? { get }
    var commentModels: [CommentModel]? { get }
    var userModel: UserModel { get }
}

extension Post: PostModel {
    var commentModels: [CommentModel]? {
        comments?.toArray()
    }

    var userModel: UserModel {
        user
    }
}
