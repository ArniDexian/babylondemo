//
//  UserModel.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 09/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

protocol UserModel: EntityModel {
    var id: String { get }
    var name: String? { get }
    var email: String? { get }
    var phone: String? { get }
    var username: String? { get }
    var website: String? { get }
    var addressModel: AddressModel? { get }
    var companyModel: UserCompanyModel? { get }
    var postModels: [PostModel]? { get }
}

extension User: UserModel {
    var addressModel: AddressModel? {
        address
    }

    var companyModel: UserCompanyModel? {
        company
    }

    var postModels: [PostModel]? {
        posts?.toArray()
    }
}
