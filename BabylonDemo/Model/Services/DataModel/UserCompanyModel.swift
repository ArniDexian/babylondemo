//
//  UserCompanyModel.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 09/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

protocol UserCompanyModel: EntityModel {
    var bs: String? { get }
    var catchPhrase: String? { get }
    var name: String? { get }
    var userModel: UserModel? { get }
}

extension UserCompany: UserCompanyModel {
    var userModel: UserModel? {
        user
    }
}
