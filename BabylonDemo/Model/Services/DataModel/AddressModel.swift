//
//  AddressModel.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 09/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

protocol AddressModel: EntityModel {
    var suite: String? { get }
    var city: String? { get }
    var street: String? { get }
    var zipCode: String? { get }
    var latitude: NSNumber? { get }
    var longitude: NSNumber? { get }
    var userModel: UserModel? { get }
}

extension Address: AddressModel {
    var userModel: UserModel? {
        user
    }
}
