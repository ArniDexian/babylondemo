//
//  PostHTTPService.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 21/07/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import ReactiveSwift

// NOTE: move to separate file
protocol PostHTTPServiceInterface {
    typealias PostsResponseHandler = (Result<[PostResponseModel], Error>) -> Void

    func requestPosts() -> SignalProducer<[PostResponseModel], Error>
}

final class PostHTTPService: HTTPService, PostHTTPServiceInterface {
    private let responseMapper: PostResponseMappable

    init(endpoint: Endpoint = .current, session: HTTPSession = URLSession.shared, responseMapper: PostResponseMappable = JSONDecoder()) {
        self.responseMapper = responseMapper
        
        super.init(endpoint: endpoint, session: session)
    }

    func requestPosts() -> SignalProducer<[PostResponseModel], Error> {
        request(path: "posts", method: .get, params: []).attemptMap { [responseMapper](data) in
            if let data = data {
                return try responseMapper.map(data: data)
            } else {
                throw MapError.mappingFailed
            }
        }
    }
}
