//
//  CommentResponseModel.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 06/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

struct CommentResponseModel: Decodable {
    let postId: Int64
    let id: Int64
    let name: String
    let email: String?
    let body: String
}
