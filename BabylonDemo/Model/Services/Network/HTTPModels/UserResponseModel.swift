//
//  UserResponseModel.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 06/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

struct UserResponseModel: Decodable {
    let id: Int64
    let name: String
    let username: String
    let email: String?
    let address: AddressResponseModel?
    let phone: String?
    let website: String?
    let company: UserCompanyResponseModel?
}
