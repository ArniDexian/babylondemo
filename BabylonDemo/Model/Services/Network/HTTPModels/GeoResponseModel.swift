//
//  GeoResponseModel.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 06/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

struct GeoResponseModel: Decodable {
    private enum CodingKeys : String, CodingKey {
        case latitude = "lng"
        case longitude = "lat"
    }

    let latitude: String
    let longitude: String
}
