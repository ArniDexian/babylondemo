//
//  PostResponseModel.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 21/07/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

struct PostResponseModel: Decodable {
    let userId: Int64
    let id: Int64
    let title: String
    let body: String
}
