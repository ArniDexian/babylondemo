//
//  UserCompanyResponseModel.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 06/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

struct UserCompanyResponseModel: Decodable {
    let name: String
    let catchPhrase: String?
    let bs: String?
}
