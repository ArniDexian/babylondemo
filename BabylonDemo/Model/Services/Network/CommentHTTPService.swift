//
//  CommentHTTPService.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 20/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import ReactiveSwift

// NOTE: move to separate file
protocol CommentHTTPServiceInterface {
    typealias CommentsResponseHandler = (Result<[CommentResponseModel], Error>) -> Void

    func requestComments() -> SignalProducer<[CommentResponseModel], Error>
}

final class CommentHTTPService: HTTPService, CommentHTTPServiceInterface {
    private let responseMapper: CommentResponseMappable

    init(endpoint: Endpoint = .current, session: HTTPSession = URLSession.shared, responseMapper: CommentResponseMappable = JSONDecoder()) {
        self.responseMapper = responseMapper

        super.init(endpoint: endpoint, session: session)
    }

    func requestComments() -> SignalProducer<[CommentResponseModel], Error> {
        request(path: "comments", method: .get, params: []).attemptMap { [responseMapper](data) in
            if let data = data {
                return try responseMapper.map(data: data)
            } else {
                throw MapError.mappingFailed
            }
        }
    }
}
