//
//  CommentResponseMapper.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 06/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

protocol CommentResponseMappable {
    func map(data: Data) throws -> [CommentResponseModel]
}

extension JSONDecoder: CommentResponseMappable {
    func map(data: Data) throws -> [CommentResponseModel] {
        try decode([CommentResponseModel].self, from: data)
    }
}
