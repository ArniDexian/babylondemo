//
//  PostResponseMapper.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 24/07/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

protocol PostResponseMappable {
    func map(data: Data) throws -> [PostResponseModel]
}

extension JSONDecoder: PostResponseMappable {
    func map(data: Data) throws -> [PostResponseModel] {
        try decode([PostResponseModel].self, from: data)
    }
}
