//
//  MapError.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 06/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

enum MapError: Error {
    case castingFailed
    case mappingFailed
}
