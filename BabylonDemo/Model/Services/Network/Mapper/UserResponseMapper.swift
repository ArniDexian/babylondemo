//
//  UserResponseMapper.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 06/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

protocol UserResponseMappable {
    func map(data: Data) throws -> [UserResponseModel]
}

extension JSONDecoder: UserResponseMappable {
    func map(data: Data) throws -> [UserResponseModel] {
        try decode([UserResponseModel].self, from: data)
    }
}
