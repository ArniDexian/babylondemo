//
//  Endpoint.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 20/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

enum Endpoint {
    case prod
    case custom(URL)

    var url: URL {
        switch self {
        case .prod:
            return URL(string: "http://jsonplaceholder.typicode.com")!

        case let .custom(url):
            return url
        }
    }

    static var current: Endpoint {
        .prod
    }
}
