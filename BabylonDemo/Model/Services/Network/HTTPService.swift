//
//  HTTPService.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 21/07/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation
import ReactiveSwift

typealias HTTPRequestParams = [(key: String, value: String)]
typealias ResponseHandler = (Result<Data?, Error>) -> Void

enum HTTPMethod: String {
    case get = "GET"
}

enum HTTPStatusCode: Int {
    case ok = 200
    case badRequest = 400
}

enum HTTPRequestError: Error {
    case urlCreation
    case requestFailed(HTTPURLResponse, Data?)
    case internalError
    case unknownStatusCode(HTTPURLResponse, Data?)
}

class HTTPService {
    private let session: HTTPSession
    private let endpoint: Endpoint

    init(endpoint: Endpoint = .current, session: HTTPSession = URLSession.shared) {
        self.endpoint = endpoint
        self.session = session
    }

    func request(path: String, method: HTTPMethod, params: HTTPRequestParams = [], completion: @escaping ResponseHandler) {

        let createRequestResult = Result { try self.createRequest(path: path, method: method, params: params) }

        switch createRequestResult {
        case let .success(urlRequest):
            let task = session.dataTask(with: urlRequest) { (data, urlResponse, error) in
                if let error = error {
                    completion(.failure(error))
                } else if let response = urlResponse as? HTTPURLResponse {
                    switch HTTPStatusCode(rawValue: response.statusCode) {
                    case .ok?:
                        completion(.success(data))
                    case .badRequest?:
                        let errorMessage = HTTPRequestError.requestFailed(response, data)
                        completion(.failure(errorMessage))
                    case .none:
                        completion(.failure(HTTPRequestError.unknownStatusCode(response, data)))
                    }
                } else {
                    completion(.failure(HTTPRequestError.internalError))
                }
            }
            task.resume()

        case let .failure(error):
            completion(.failure(error))
        }
    }

    private func createRequest(path: String, method: HTTPMethod, params: HTTPRequestParams) throws -> URLRequest {
        var request: URLRequest!

        let url = endpoint.url.appendingPathComponent(path)
        switch method {
        case .get:
            guard var components = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
                throw HTTPRequestError.urlCreation
            }
            components.queryItems =
                params.map({ URLQueryItem(name: $0.key, value: $0.value) })

            guard let createdURL = components.url else {
                // impossible scenario withinin this block right now
                preconditionFailure("Can't create url from components \(components)")
            }

            request = URLRequest(url: createdURL)
            request.httpMethod = method.rawValue
        }

        return request
    }
}

/// Reactive requests
// NOTE: move to separate extension
extension HTTPService {
    func request(path: String, method: HTTPMethod, params: HTTPRequestParams = []) -> SignalProducer<Data?, Error> {
        return SignalProducer { [weak self](observer, lifetime) in
            guard let self = self else {
                observer.sendInterrupted()
                return
            }

            self.request(path: path, method: method, params: params) { result in
                guard !lifetime.hasEnded else { return }

                switch result {
                case let .success(data):
                    observer.send(value: data)
                    observer.sendCompleted()

                case let .failure(error):
                    observer.send(error: error)
                }
            }
        }
    }
}
