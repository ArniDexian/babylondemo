//
//  UserHTTPService.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 06/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import ReactiveSwift

// NOTE: move to separate file
protocol UserHTTPServiceInterface {
    typealias PostsResponseHandler = (Result<[UserResponseModel], Error>) -> Void

    func requestUsers() -> SignalProducer<[UserResponseModel], Error>
}

final class UserHTTPService: HTTPService, UserHTTPServiceInterface {
    private let responseMapper: UserResponseMappable

    init(endpoint: Endpoint = .current, session: HTTPSession = URLSession.shared, responseMapper: UserResponseMappable = JSONDecoder()) {
        self.responseMapper = responseMapper

        super.init(endpoint: endpoint, session: session)
    }

    func requestUsers() -> SignalProducer<[UserResponseModel], Error> {
        request(path: "users", method: .get, params: []).attemptMap { [responseMapper](data) in
            if let data = data {
                return try responseMapper.map(data: data)
            } else {
                throw MapError.mappingFailed
            }
        }
    }
}
