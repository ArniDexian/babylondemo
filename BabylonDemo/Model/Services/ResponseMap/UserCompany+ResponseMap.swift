//
//  UserCompany+ResponseMap.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 06/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import CoreData

extension UserCompany {
    func map(from response: UserCompanyResponseModel, dataStorage: DataStorage) {
        name = response.name
        catchPhrase = response.catchPhrase
        bs = response.bs
    }
}
