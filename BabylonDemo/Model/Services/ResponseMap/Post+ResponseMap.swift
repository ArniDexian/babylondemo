//
//  Post+ResponseMap.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 08/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

extension Post {
    func map(from response: PostResponseModel, dataStorage: DataStorage) {
        guard let context = managedObjectContext else {
            preconditionFailure("\(#function) failed to map response on object without context")
        }

        id = String(response.id)

        if let existingUser = dataStorage.get(User.self, with: NSPredicate(format: "id == %@", String(response.userId)), in: context)?.first {
            user = existingUser
        } else if let createdUser = dataStorage.create(User.self, context: context) {
            createdUser.id = String(response.userId)
            user = createdUser
        }

        body = response.body
        title = response.title
    }
}
