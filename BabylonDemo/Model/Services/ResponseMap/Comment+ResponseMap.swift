//
//  Comment+ResponseMap.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 20/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import CoreData

extension Comment {
    func map(from response: CommentResponseModel, dataStorage: DataStorage) {
        guard let context = managedObjectContext else {
            preconditionFailure("\(#function) failed to map response on object without context")
        }

        self.id = String(response.id)
        post = dataStorage.get(Post.self, with: NSPredicate(format: "id == %@", String(response.postId)), in: context)?.first
        name = response.name
        email = response.email
        body = response.body
    }
}
