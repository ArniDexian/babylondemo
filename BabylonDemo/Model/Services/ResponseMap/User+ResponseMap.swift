//
//  UserModel+ResponseMap.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 06/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import CoreData

extension User {
    func map(from response: UserResponseModel, dataStorage: DataStorage) {
        guard let context = managedObjectContext else {
            preconditionFailure("\(#function) failed to map response on object without context")
        }

        id = String(response.id)
        name = response.name
        username = response.username
        email = response.email
        phone = response.phone
        website = response.website

        if let respAddress = response.address {
            if address == nil {
                address = dataStorage.create(Address.self, context: context)
            }

            address?.map(from: respAddress, dataStorage: dataStorage)
        } else if let existing = address {
            context.delete(existing)
        }

        if let respCompany = response.company {
            if company == nil {
                company = dataStorage.create(UserCompany.self, context: context)
            }

            company?.map(from: respCompany, dataStorage: dataStorage)
        } else if let existing = company {
            context.delete(existing)
        }
    }
}
