//
//  Address+ResponseMap.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 06/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import CoreData

extension Address {
    func map(from response: AddressResponseModel, dataStorage: DataStorage) {
        street = response.street
        suite = response.suite
        city = response.city
        zipCode = response.zipcode
        
        if let loc = response.geo {
            latitude = Double(loc.latitude) as NSNumber?
            longitude = Double(loc.latitude) as NSNumber?
        } else {
            latitude = nil
            longitude = nil
        }
    }
}
