//
//  CommentService.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 20/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import CoreData
import ReactiveSwift

enum CommentServiceError: Error {
    case dataStorageFailure(Error?)
}

// NOTE: move to separate file under Contracts group
protocol CommentServiceInterface {
    func fetchComments() -> SignalProducer<[CommentModel], Error>
}

final class CommentService: CommentServiceInterface {
    private let dataStorage: DataStorage
    private let httpService: CommentHTTPServiceInterface

    init(dataStorage: DataStorage, httpService: CommentHTTPServiceInterface = CommentHTTPService()) {
        self.dataStorage = dataStorage
        self.httpService = httpService
    }

    func fetchComments() -> SignalProducer<[CommentModel], Error> {
        httpService.requestComments().flatMap(.merge) { response in
            self.saveToStorage(response: response)
        }
    }

    // NOTE: subject to re-think and reuse across all model services
    private func saveToStorage(response: [CommentResponseModel]) -> SignalProducer<[CommentModel], Error> {
        SignalProducer { [dataStorage](observer, lifetime) in
            let ctx = dataStorage.newBackgroundContext()

            ctx.perform {
                let comments = response.compactMap { (commentResponse) -> Comment? in
                    let predicate = NSPredicate(format: "id == %@", String(commentResponse.id))
                    guard let comment = dataStorage.get(Comment.self, with: predicate, in: ctx)?.first ?? dataStorage.create(Comment.self, context: ctx) else {
                        assertionFailure("failed to map comment \(commentResponse)")
                        return nil
                    }

                    comment.map(from: commentResponse, dataStorage: dataStorage)

                    guard comment.post != nil else {
                        print("failed to find post with id \(commentResponse.postId), deleting comment \(commentResponse.id)")
                        ctx.delete(comment)
                        return nil
                    }

                    return comment
                }

                do {
                    try ctx.saveAndReset()
                    
                    let updated = dataStorage.refreshedObjects(comments)
                    observer.send(value: updated ?? [])
                    observer.sendCompleted()
                } catch {
                    observer.send(error: error)
                }
            }
        }
    }
}
