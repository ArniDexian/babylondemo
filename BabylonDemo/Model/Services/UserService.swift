//
//  UserService.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 27/07/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import CoreData
import ReactiveSwift

enum UserServiceError: Error {
    case dataStorageFailure(Error?)
}

// NOTE: move to separate file under Contracts group
protocol UserServiceInterface {
    func fetchUsers() -> SignalProducer<[UserModel], Error>
}

final class UserService: UserServiceInterface {
    private let dataStorage: DataStorage
    private let httpService: UserHTTPServiceInterface

    init(dataStorage: DataStorage, httpService: UserHTTPServiceInterface = UserHTTPService()) {
        self.dataStorage = dataStorage
        self.httpService = httpService
    }

    func fetchUsers() -> SignalProducer<[UserModel], Error> {
        httpService.requestUsers().flatMap(.merge) { response in
            self.saveToStorage(response: response)
        }
    }

    // NOTE: subject to re-think and reuse across all model services
    private func saveToStorage(response: [UserResponseModel]) -> SignalProducer<[UserModel], Error> {
        SignalProducer { [dataStorage](observer, lifetime) in
            let ctx = dataStorage.newBackgroundContext()

            ctx.perform {
                let users = response.compactMap { (userResponse) -> User? in
                    let predicate = NSPredicate(format: "id == %@", String(userResponse.id))
                    guard let user = dataStorage.get(User.self, with: predicate, in: ctx)?.first ?? dataStorage.create(User.self, context: ctx) else {
                        assertionFailure("failed to map user \(userResponse)")
                        return nil
                    }

                    user.map(from: userResponse, dataStorage: dataStorage)

                    return user
                }

                do {
                    try ctx.saveAndReset()

                    let updated = dataStorage.refreshedObjects(users)
                    observer.send(value: updated ?? [])
                    observer.sendCompleted()
                } catch {
                    observer.send(error: error)
                }
            }
        }
    }
}
