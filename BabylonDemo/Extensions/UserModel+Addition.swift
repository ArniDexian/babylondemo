//
//  UserModel+Addition.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 22/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

extension UserModel {
    var displayName: String {
        return name ?? username ?? id
    }
}
