//
//  Data+Extension.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 21/07/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

extension Data {
    var utf8String: String? {
        return String(data: self, encoding: .utf8)
    }
}
