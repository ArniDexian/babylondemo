//
//  String+Extension.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 09/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import Foundation

extension String {
    var double: Double? {
        return Double(self)
    }
}

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
