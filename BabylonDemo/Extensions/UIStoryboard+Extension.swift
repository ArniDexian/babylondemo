//
//  UIStoryboard+Extension.swift
//  BabylonDemo
//
//  Created by Arni Dexian on 28/08/2019.
//  Copyright © 2019 ArniDexian. All rights reserved.
//

import UIKit

extension UIStoryboard {
    func instantiateViewController<V: UIViewController>(_ type: V.Type) -> V {
        guard let viewController = instantiateViewController(withIdentifier: "\(V.self)") as? V else {
            fatalError("\(#function) failed to cast view controller to type \(V.self)")
        }

        return viewController
    }
}
